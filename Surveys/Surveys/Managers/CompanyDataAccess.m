//
//  CompanyDataAccess.m
//  Surveys
//
//  Created by Raúl Blánquez on 29/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "CompanyDataAccess.h"
#import "AppDelegate.h"
#import "DataManager.h"
#import "GlobalHeader.h"
#import "WebServices.h"
#import "Message.h"

@implementation CompanyDataAccess

// *****************************************************************************
#pragma mark -                                          Singleton stuff
// *****************************************************************************
+ (CompanyDataAccess *)sharedManager
{
    static CompanyDataAccess *sharedObject = nil;
    static dispatch_once_t singletonPredicate;
    
    dispatch_once(&singletonPredicate, ^{
        sharedObject = [[super allocWithZone:nil] init];
    });
    
    return sharedObject;
}

// *****************************************************************************
#pragma mark -                                          Public methods
// *****************************************************************************
- (Company *)getCompany
{
    NSArray *results = [DataManager getFromEntity:kCompanyEntity withPredicate:nil andSortBy:Nil ascending:YES];
    return results.firstObject;
}

- (Company *)setCompany:(NSDictionary *)companyDict
{
    NSDictionary *info = [companyDict[kResponseKeyResponseObject] firstObject];
    
    if (info)
    {
        NSNumber *companyId = info[@"entity"] ? @([info[@"entity"] integerValue]) : @1;
        NSString *name      = info[@"name"] ? info[@"name"] : @"Restaurant";
        NSString *phone     = info[@"phone"] ? info[@"phone"] : @"";
        NSString *address   = info[@"address"] ? info[@"address"] : @"";
        NSString *city      = info[@"city"] ? info[@"city"] : @"";
        NSNumber *customers = info[@"maxpax"] ? @([info[@"maxpax"] integerValue]) : @6;
        NSNumber *tables    = info[@"tables"] ? @([info[@"tables"] integerValue]) : @10;
        NSArray *messages   = info[@"texts"] ? info[@"texts"] : @[];
        NSArray *languages  = info[@"langs"] ? info[@"langs"] : @[];
        NSString *email     = info[@"email"] ? info[@"email"] : @"";
        
        return  [DataManager setCompany:[self getCompany] idCompany:companyId name:name mail:email phone:phone address:address city:city customers:customers tables:tables messages:messages languages:languages];
    }

    return nil;
}

- (NSNumber *)getCurrentSurvey
{
    return [[self getCompany] currentSurveyId];
}

- (NSString *)getWellcomeMessageForLanguage:(NSString *)languageCode
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"languageCode == %@", languageCode];
    
    NSArray *results = [DataManager getFromEntity:kMessageEntity withPredicate:predicate andSortBy:Nil ascending:YES];

    Message *message = results.firstObject;
    
    return message.welcome;
}

- (NSString *)getThanksMessageForLanguage:(NSString *)languageCode
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"languageCode == %@", languageCode];
    
    NSArray *results = [DataManager getFromEntity:kMessageEntity withPredicate:predicate andSortBy:Nil ascending:YES];
    
    Message *message = results.firstObject;
    
    return message.thanks;
}

@end

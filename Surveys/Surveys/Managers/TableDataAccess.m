//
//  TableDataAccess.m
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "TableDataAccess.h"
#import "DataManager.h"
#import "GlobalHeader.h"

@implementation TableDataAccess

+ (NSArray *)getTables
{
    return [DataManager getFromEntity:kTableEntity
                        withPredicate:nil
                            andSortBy:@"tableId"
                            ascending:YES];
}

@end

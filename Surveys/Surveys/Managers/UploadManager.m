//
//  UploadManager.m
//  Surveys
//
//  Created by Raúl Blánquez on 8/12/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "UploadManager.h"
#import "CompanyDataAccess.h"
#import "SurveyDataAccess.h"
#import "HistoryDataAccess.h"
#import "GlobalHeader.h"
#import "WebServices.h"
#import "NSDictionary+JSON.h"
#import "SVProgressHUD.h"

@interface UploadManager()
{
    NSInteger _questionCounter;
    NSArray *_questions;
    NSNumber *_surveyId;
    NSNumber *_companyId;
}

@end

@implementation UploadManager

// *****************************************************************************
#pragma mark -                                          Singleton stuff
// *****************************************************************************
+ (UploadManager *)sharedManager
{
    static UploadManager *sharedObject = nil;
    static dispatch_once_t singletonPredicate;
    
    dispatch_once(&singletonPredicate, ^{
        sharedObject = [[super allocWithZone:nil] init];
    });
    
    return sharedObject;
}

// *****************************************************************************
#pragma mark -                                          WebService Notifications
// *****************************************************************************
- (void)didUploadQuestion:(NSNotification *)notification
{
    NSDictionary *responseDict = notification.userInfo;
    BOOL success = [[responseDict objectForKey:kResponseKeySuccess] boolValue];
    id responseObject = [responseDict objectForKey:kResponseKeyResponseObject];
    if (success)
    {
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                             options:kNilOptions
                                                               error:&error];
        
        if (!json)
        {
            NSString *receivedString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:receivedString
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                                  otherButtonTitles:nil];
            [alert show];
            [SVProgressHUD dismiss];
            return;
        }

        if (_questionCounter == _questions.count -1)
        {
            DLog(@"All surveys uploaded");
            [[NSNotificationCenter defaultCenter] removeObserver:self
                                                            name:kWSNotificationHistory
                                                          object:nil];
            [SVProgressHUD dismiss];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Nil
                                                            message:NSLocalizedString(@"upload_ok_label", Nil)
                                                           delegate:Nil
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
            [self.delegate didFinishedUploadingSurvey];
        }
        else
        {
            DLog(@"Uploading survey %d", (int)_questionCounter+1);
            _questionCounter++;
            [self uploadQuestion];
        }
    }
    else
    {
        DLog(@"Error uploading survey");
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:kWSNotificationHistory
                                                      object:nil];
        [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Nil
                                                        message:NSLocalizedString(@"upload_error_label", Nil)
                                                       delegate:Nil
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
}

// *****************************************************************************
#pragma mark -                                          Public Methods
// *****************************************************************************
- (void)uploadHistoryForCurrentSurvey
{
    _surveyId = [[CompanyDataAccess sharedManager] getCurrentSurvey];
    _companyId= [[CompanyDataAccess sharedManager] getCompany].idCompany;
    
    _questions = [[SurveyDataAccess sharedManager] getQuestionsForDeviceLanguage];
    
    _questionCounter = 0;
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"stats_uploading_label", Nil) maskType:SVProgressHUDMaskTypeClear];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didUploadQuestion:)
                                                 name:kWSNotificationHistory
                                               object:nil];
    
    [self uploadQuestion];
}

- (void)uploadQuestion
{
    Question *question = _questions[_questionCounter];
    
    NSDictionary *history;
    if ([question.format integerValue] == kQuestionComments)
    {
        history = [[HistoryDataAccess sharedManager] getCommentsForQuestion:question.questionId];
    }
    else
    {
        history = [[HistoryDataAccess sharedManager] getHistoryForQuestion:question.questionId];
    }
    
    NSDictionary *params = @{kWSAction          : kWSActionSend,
                             kWSActionEntity    : [NSString stringWithFormat:@"%@", _companyId],
                             kWSActionSurvey    : [NSString stringWithFormat:@"%@", _surveyId]};
    
    DLog(@"%@", params);
    
    NSDictionary *bodyDict = @{kWSActionSend : history};
    
    NSString *bodyString = [bodyDict jsonStringWithPrettyPrint:NO];
    NSData *body = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    [WebServices makePostRequestToURL:[WebServices stringUrlForPathKey:kPathKeySetData]
                       withParameters:params
                             withBody:body
                   withNotificationId:kWSNotificationHistory];
}

@end

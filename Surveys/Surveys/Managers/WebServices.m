//
//  WebServices.m
//  Surveys
//
//  Created by Raúl Blánquez on 2/11/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "WebServices.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "GlobalHeader.h"
#import "DeviceUtils.h"

#define PLIST_FILE_NAME                 @"SurveysWebServices"

#define kPlistKeyServerUrl              @"serverUrl"

#define kRequestHeaderNameAppCode       @"APP_CODE"
#define kRequestHeaderNameAppVersion    @"APP_VERSION"
#define kRequestHeaderNameAppTime       @"APP_TIME"
#define kRequestHeaderNameLanguage      @"LANGUAGE"
#define kRequestHeaderNameOSType        @"OSTYPE"
#define kRequestHeaderNameOSVersion     @"OSVERSION"
#define kRequestHeaderNameDeviceID      @"DEVICE_ID"

#define kRequestHeaderValueOSType       @"iOS"

#define kRequestServiceVersion          @"XXX"

@implementation WebServices

// *****************************************************************************
#pragma mark -                                          Generic Web Services
// *****************************************************************************
+ (void)makeGetRequestToURL:(NSString *)urlString
             withParameters:(NSDictionary *)parameters
                   withBody:(NSData *)body
         withNotificationId:(NSString *)notificationId
{
    
    NSMutableDictionary *headers = [self defaultRequestHeaders];

    NSString *urlStringWithVersion = [urlString stringByReplacingOccurrencesOfString:kRequestServiceVersion
                                                                          withString:[DeviceUtils appVersion]];
    
    [self makeRequestToUrl:urlStringWithVersion
                withMethod:@"GET"
            withParameters:parameters
               withHeaders:headers
                  withBody:body
        withNotificationId:notificationId];
}

+ (void)makePostRequestToURL:(NSString *)urlString
              withParameters:(NSDictionary *)parameters
                    withBody:(NSData *)body
          withNotificationId:(NSString *)notificationId
{
    
    NSMutableDictionary *headers = [self defaultRequestHeaders];
    
    [headers setObject:kRequestHeaderValueContentTypeJSON
                forKey:kRequestHeaderNameContentType];

    NSString *urlStringWithVersion = [urlString stringByReplacingOccurrencesOfString:kRequestServiceVersion
                                                                          withString:[DeviceUtils appVersion]];
    
    [self makeRequestToUrl:urlStringWithVersion
                withMethod:@"POST"
            withParameters:parameters
               withHeaders:headers
                  withBody:body
        withNotificationId:notificationId];
}

// *****************************************************************************
#pragma mark -                                          Headers configuration
// *****************************************************************************
+ (NSMutableDictionary *)defaultRequestHeaders
{
    NSMutableDictionary *headers = [[NSMutableDictionary alloc] init];
    
    [headers setObject:[DeviceUtils appVersion]
                forKey:kRequestHeaderNameAppVersion];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    [headers setObject:[dateFormatter stringFromDate:[NSDate date]]
                forKey:kRequestHeaderNameAppTime];
    
    [headers setObject:[DeviceUtils deviceLanguage]
                forKey:kRequestHeaderNameLanguage];
    
    [headers setObject:kRequestHeaderValueOSType
                forKey:kRequestHeaderNameOSType];
    
    [headers setObject:[DeviceUtils deviceOSVersion]
                forKey:kRequestHeaderNameOSVersion];
    
    [headers setObject:[DeviceUtils deviceID]
                forKey:kRequestHeaderNameDeviceID];
    
    return headers;
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
+ (void)makeRequestToUrl:(NSString *)urlString
              withMethod:(NSString *)method
          withParameters:(NSDictionary *)parameters
             withHeaders:(NSDictionary *)headers
                withBody:(NSData *)body
      withNotificationId:(NSString *)notificationId
{
    NSString *completeUrlString = urlString;
    
    // Request parameters
    NSString *queryString = [self _buildQueryStringFromDictionary:parameters];
    if (queryString.length)
    {
        NSString *separator = @"&";
        NSRange questionRange = [completeUrlString rangeOfString:@"?"];
        if (questionRange.location == NSNotFound) {
            separator = @"?";
        }
        completeUrlString = [completeUrlString stringByAppendingFormat:@"%@%@", separator, queryString];
    }
    
    NSURL *URL = [NSURL URLWithString:completeUrlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    
    // Request method
    request.HTTPMethod = method;
    
    // Request headers
    
    // Add charset to Content-Type header
    NSString *contentTypeValue = [headers objectForKey:kRequestHeaderNameContentType];
    if (!contentTypeValue.length)
    {
        contentTypeValue = kRequestHeaderValueContentTypeTextPlain;
    }
    
    contentTypeValue = [contentTypeValue stringByAppendingFormat:@"; %@", kRequestHeaderValueCharsetUTF8];
    NSMutableDictionary *modifiedHeaders = [NSMutableDictionary dictionaryWithDictionary:headers];
    [modifiedHeaders setObject:contentTypeValue
                        forKey:kRequestHeaderNameContentType];
    
    for (NSString *headerName in modifiedHeaders.allKeys)
    {
        NSString *headerValue = [modifiedHeaders objectForKey:headerName];
        [request setValue:headerValue forHTTPHeaderField:headerName];
    }
    
    // Request body
    request.HTTPBody = body;
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:request];
    
    DLog(@"%@", request);
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
#ifdef DEBUG
        NSString *receivedString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        DLog(@"Received: %@", receivedString);
#endif
        NSMutableDictionary *responseDict = [[NSMutableDictionary alloc] init];
        [responseDict setObject:[NSNumber numberWithBool:YES]
                         forKey:kResponseKeySuccess];
        
        if (responseObject)
        {
            [responseDict setObject:responseObject
                             forKey:kResponseKeyResponseObject];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationId
                                                            object:nil
                                                          userInfo:responseDict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        DLog(@"There was an error with you request: %@", error.localizedDescription);
        
        NSMutableDictionary *responseDict = [[NSMutableDictionary alloc] init];
        [responseDict setObject:[NSNumber numberWithBool:NO]
                         forKey:kResponseKeySuccess];
        
        if (error)
        {
            [responseDict setObject:error.localizedDescription
                             forKey:kResponseKeyResponseObject];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationId
                                                            object:nil
                                                          userInfo:responseDict];
        
        
    }];
    [operation start];
}

+ (NSString *)_buildQueryStringFromDictionary:(NSDictionary *)dictionary
{
    NSString *queryString = nil;
    NSArray *paramNames = [dictionary allKeys];
    NSInteger numParams = paramNames.count;
    
    for (int i=0; i < numParams; i++)
    {
        if (!queryString)
        {
            queryString = @"";
        }
        else
        {
            queryString = [queryString stringByAppendingString:@"&"];
        }
        
        NSString *paramName = [paramNames objectAtIndex:i];
        NSString *paramValue = [dictionary objectForKey:paramName];
        paramValue = [paramValue stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        queryString = [queryString stringByAppendingFormat:@"%@=%@", paramName, paramValue];
    }
    
    return queryString;
}

+ (NSString *)stringUrlForPathKey:(NSString *)pathKey
{
    NSDictionary *plistDict = [self _dictFromPlist];
    NSString *path = [plistDict objectForKey:pathKey];
    
    return [self stringUrlForPathString:path];
}

+ (NSString *)stringUrlForPathString:(NSString *)pathString
{
    if (!pathString.length) {
        return nil;
    }
    
    NSDictionary *plistDict = [self _dictFromPlist];
    NSString *serverUrl = [plistDict objectForKey:kPlistKeyServerUrl];
    
    NSString *ret = serverUrl;
    
    ret = [ret stringByAppendingString:pathString];
    
    return ret;
}

+ (BOOL)internetAvailability
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable;
}

+ (NSDictionary *)_dictFromPlist
{
    NSString *path = [[NSBundle mainBundle] pathForResource:PLIST_FILE_NAME
                                                     ofType:@"plist"];
    
    NSDictionary *ret = [[NSDictionary alloc] initWithContentsOfFile:path];
    return ret;
}

@end

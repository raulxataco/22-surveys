//
//  LanguageDataAccess.m
//  Surveys
//
//  Created by Raúl Blánquez on 02/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "LanguageDataAccess.h"
#import "DataManager.h"
#import "GlobalHeader.h"

@implementation LanguageDataAccess

+ (NSArray *)getLanguages
{
    return [DataManager getFromEntity:kLanguageEntity
                        withPredicate:nil
                            andSortBy:Nil
                            ascending:YES];
}

+ (Language *)getLanguage:(NSString *)languageCode
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"languageCode = %@", languageCode];
    
    NSArray *results = [DataManager getFromEntity:kLanguageEntity withPredicate:predicate andSortBy:Nil ascending:YES];
    
    return results.firstObject;
}

+ (NSString *)getLanguageCodeForLanguageId:(NSNumber *)languageId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"languageId == %@", languageId];
    NSArray *results = [DataManager getFromEntity:kLanguageEntity withPredicate:predicate andSortBy:nil ascending:YES];
    
    Language *language = results.firstObject;

    return language.languageCode ? language.languageCode : @"es";
}

+ (NSNumber *)getLanguageIdForLanguageCode:(NSString *)languageCode
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"languageCode == %@", languageCode];
    NSArray *results = [DataManager getFromEntity:kLanguageEntity withPredicate:predicate andSortBy:nil ascending:YES];
    
    Language *language = results.firstObject;
    
    return language.languageId ? language.languageId : @0;
}

@end

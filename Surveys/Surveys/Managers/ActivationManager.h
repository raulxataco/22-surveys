//
//  ActivationManager.h
//  Surveys
//
//  Created by Raúl Blánquez on 22/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Company.h"

@protocol ActivationManagerDelegate
- (void)didFinishActivation;
@optional
- (void)didFailActivation;
@end

@interface ActivationManager : NSObject

@property (nonatomic, weak) id<ActivationManagerDelegate> delegate;
@property (nonatomic, strong) Company *currentCompany;

+ (ActivationManager *)sharedManager;

- (void)startActivationForEntity:(NSString *)entityId andMail:(NSString *)email;
- (void)updateActivationForEntity:(NSString *)entityId;

@end

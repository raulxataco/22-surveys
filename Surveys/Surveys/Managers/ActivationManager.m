//
//  ActivationManager.m
//  Surveys
//
//  Created by Raúl Blánquez on 22/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "ActivationManager.h"

#import "GlobalHeader.h"
#import "CompanyDataAccess.h"
#import "ServerDataAccess.h"
#import "SurveyDataAccess.h"
#import "WebServices.h"
#import "SVProgressHUD.h"

@interface ActivationManager ()
{
    NSMutableArray *_surveysList;
    Survey *_currentSurvey;
}

@end

@implementation ActivationManager

// *****************************************************************************
#pragma mark -                                          Singleton stuff
// *****************************************************************************
+ (ActivationManager *)sharedManager
{
    static ActivationManager *sharedObject = nil;
    static dispatch_once_t singletonPredicate;
    
    dispatch_once(&singletonPredicate, ^{
        sharedObject = [[super allocWithZone:nil] init];
    });
    
    return sharedObject;
}

// *****************************************************************************
#pragma mark -                                          Public Methods
// *****************************************************************************
- (void)startActivationForEntity:(NSString *)entityId andMail:(NSString *)email
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"main_company_download_label", Nil) maskType:SVProgressHUDMaskTypeClear];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didDownloadEntityInfo:)
                                                 name:kWSNotificationEntityInfo
                                               object:nil];
    
    [WebServices makeGetRequestToURL:[WebServices stringUrlForPathKey:kPathKeyGetData]
                      withParameters:@{kWSAction:kWSActionEntity, kWSEntityId:entityId, kWSEmailId:email}
                            withBody:nil
                  withNotificationId:kWSNotificationEntityInfo];
}

- (void)updateActivationForEntity:(NSString *)entityId
{
    
}

// *****************************************************************************
#pragma mark -                                          Webservice Notifications
// *****************************************************************************
- (void)didDownloadEntityInfo:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kWSNotificationEntityInfo
                                                  object:nil];
    
    NSDictionary *responseDict = notification.userInfo;
    BOOL success = [[responseDict objectForKey:kResponseKeySuccess] boolValue];
    id responseObject = [responseDict objectForKey:kResponseKeyResponseObject];
    NSString *errorMsg;
    if (success)
    {
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                             options:kNilOptions
                                                               error:&error];
        if (!error)
        {
            DLog(@"%@", json);
            
            NSString *entity = json[@"response"][0][@"entity"];
            if ([entity isEqualToString:@"Authentication failed"])
            {
                [SVProgressHUD dismiss];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", Nil)
                                                                message:NSLocalizedString(@"invalid_company", Nil)
                                                               delegate:Nil
                                                      cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                                      otherButtonTitles:nil];
                [alert show];

                return;
            }
            
            self.currentCompany = [[CompanyDataAccess sharedManager] setCompany:json];
            
            [self _requestServersForCompany:self.currentCompany];
            
            return;
        }
        else
        {
            DLog(@"Error en formato json");
            errorMsg = @"error en formato json";
            [SVProgressHUD dismiss];
        }
    }
    else
    {
        DLog(@"%@", responseObject);
        
        errorMsg = responseObject;
        
        [SVProgressHUD dismiss];
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", Nil)
                                                    message:errorMsg
                                                   delegate:Nil
                                          cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                          otherButtonTitles:nil];
    [alert show];
    
}

- (void)didDownloadServersInfo:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kWSNotificationServersInfo
                                                  object:nil];
    
    NSDictionary *responseDict = notification.userInfo;
    BOOL success = [[responseDict objectForKey:kResponseKeySuccess] boolValue];
    id responseObject = [responseDict objectForKey:kResponseKeyResponseObject];
    NSString *errorMsg;
    if (success)
    {
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                             options:kNilOptions
                                                               error:&error];
        if (!error)
        {
            DLog(@"%@", json);
            [[ServerDataAccess sharedManager] setServers:json forCompany:self.currentCompany];
            
            [self _requestSurveysForCompany:self.currentCompany];
            
            return;
        }
        else
        {
            DLog(@"Error en formato json");
            
            errorMsg = @"Error en formato json";
            
            [SVProgressHUD dismiss];
        }
    }
    else
    {
        DLog(@"%@", responseObject);
        
        errorMsg = responseObject;
        
        [SVProgressHUD dismiss];
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", Nil)
                                                    message:errorMsg
                                                   delegate:Nil
                                          cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)didDownloadSurveysInfo:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kWSNotificationSurveysInfo
                                                  object:nil];
    
    NSDictionary *responseDict = notification.userInfo;
    BOOL success = [[responseDict objectForKey:kResponseKeySuccess] boolValue];
    id responseObject = [responseDict objectForKey:kResponseKeyResponseObject];
    NSString *errorMsg;
    if (success)
    {
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                             options:kNilOptions
                                                               error:&error];
        if (!error)
        {
            DLog(@"%@", json);
            [[SurveyDataAccess sharedManager] setSurveys:json forCompany:_currentCompany];
            
            _surveysList = [NSMutableArray arrayWithArray:[[SurveyDataAccess sharedManager] getSurveys]];
            
            [self _requestQuestions];
            
            return;
        }
        else
        {
            DLog(@"Error en formato json");
            
            errorMsg = @"error en formato json";
            
            [SVProgressHUD dismiss];
        }
    }
    else
    {
        DLog(@"%@", responseObject);
        
        errorMsg = responseObject;
        
        [SVProgressHUD dismiss];
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", Nil)
                                                    message:errorMsg
                                                   delegate:Nil
                                          cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)didDownloadQuestionsInfo:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kWSNotificationQuestionsInfo
                                                  object:nil];
    
    NSDictionary *responseDict = notification.userInfo;
    BOOL success = [[responseDict objectForKey:kResponseKeySuccess] boolValue];
    id responseObject = [responseDict objectForKey:kResponseKeyResponseObject];
    NSString *errorMsg;
    if (success)
    {
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                             options:kNilOptions
                                                               error:&error];
        if (!error)
        {
            DLog(@"%@", json);
            [[SurveyDataAccess sharedManager] setQuestions:json
                                                 forSurvey:_currentSurvey];
            
            [_surveysList removeObjectAtIndex:0];
            
            [self _requestQuestions];
            
            return;
        }
        else
        {
            DLog(@"Error en formato json");
            
            errorMsg = @"error en formato json";
            
            [SVProgressHUD dismiss];
        }
    }
    else
    {
        DLog(@"%@", responseObject);
        
        errorMsg = responseObject;
        
        [SVProgressHUD dismiss];
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", Nil)
                                                    message:errorMsg
                                                   delegate:Nil
                                          cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)_requestServersForCompany:(Company *)company
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"main_servers_download_label", Nil) maskType:SVProgressHUDMaskTypeClear];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didDownloadServersInfo:)
                                                 name:kWSNotificationServersInfo
                                               object:nil];
    
    [WebServices makeGetRequestToURL:[WebServices stringUrlForPathKey:kPathKeyGetData]
                      withParameters:@{kWSAction:kWSActionPeople, kWSEntityId: [NSString stringWithFormat:@"%@", _currentCompany.idCompany]}
                            withBody:nil
                  withNotificationId:kWSNotificationServersInfo];
}

- (void)_requestSurveysForCompany:(Company *)company
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"main_surveys_download_label", Nil) maskType:SVProgressHUDMaskTypeClear];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didDownloadSurveysInfo:)
                                                 name:kWSNotificationSurveysInfo
                                               object:nil];
    
    [WebServices makeGetRequestToURL:[WebServices stringUrlForPathKey:kPathKeyGetData]
                      withParameters:@{kWSAction:   kWSActionSurveys,
                                       kWSEntityId: [NSString stringWithFormat:@"%@", _currentCompany.idCompany]}
                            withBody:nil
                  withNotificationId:kWSNotificationSurveysInfo];
}

- (void)_requestQuestions
{
    if (_surveysList.count == 0)
    {
        DLog(@"Database correctly created...");
        
        [SVProgressHUD dismiss];
        
        [self.delegate didFinishActivation];
        
        return;
    }
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"main_questions_download_label", Nil) maskType:SVProgressHUDMaskTypeClear];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didDownloadQuestionsInfo:)
                                                 name:kWSNotificationQuestionsInfo
                                               object:nil];
    
    _currentSurvey = [_surveysList firstObject];
    
    [WebServices makeGetRequestToURL:[WebServices stringUrlForPathKey:kPathKeyGetData]
                      withParameters:@{kWSAction:   kWSActionQuestions,
                                       kWSSurveyId: [NSString stringWithFormat:@"%@", _currentSurvey.surveyId]}
                            withBody:nil
                  withNotificationId:kWSNotificationQuestionsInfo];
}

@end

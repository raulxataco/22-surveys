//
//  UploadManager.h
//  Surveys
//
//  Created by Raúl Blánquez on 8/12/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol UploadSurveyDelegate
- (void)didFinishedUploadingSurvey;
@end

@interface UploadManager : NSObject

@property (nonatomic, weak) id<UploadSurveyDelegate> delegate;

+ (UploadManager *)sharedManager;

- (void)uploadHistoryForCurrentSurvey;

@end

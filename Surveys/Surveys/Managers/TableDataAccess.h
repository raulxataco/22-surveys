//
//  TableDataAccess.h
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TableDataAccess : NSObject

+ (NSArray *)getTables;

@end

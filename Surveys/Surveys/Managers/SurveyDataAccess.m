//
//  SurveyDataAccess.m
//  Surveys
//
//  Created by Raúl Blánquez on 01/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "SurveyDataAccess.h"
#import "CompanyDataAccess.h"
#import "LanguageDataAccess.h"
#import "DataManager.h"
#import "GlobalHeader.h"
#import "WebServices.h"

@implementation SurveyDataAccess

// *****************************************************************************
#pragma mark -                                          Singleton stuff
// *****************************************************************************
+ (SurveyDataAccess *)sharedManager
{
    static SurveyDataAccess *sharedManager = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedManager = [[SurveyDataAccess alloc] init];
        sharedManager.languageCode = @"es";
    });
    
    return sharedManager;
}

// *****************************************************************************
#pragma mark -                                          Public Methods
// *****************************************************************************
- (NSArray *)getSurveys
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"languageCode == %@", @"es"];

    return [DataManager getFromEntity:kSurveyEntity
                        withPredicate:predicate
                            andSortBy:Nil
                            ascending:YES];
}

- (Survey *)getFirstSurvey
{
    NSArray *results = [self getSurveys];
    
    return results.firstObject;
}

//- (Survey *)getSurveyWithLanguageId:(NSNumber *)languageId
//{
//    NSString *languageCode = [LanguageDataAccess getLanguageCodeForLanguageId:languageId];
//    
//    NSNumber *surveyId = [[CompanyDataAccess sharedManager] getCurrentSurvey];
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"surveyId == %@ && languageCode == %@", surveyId, languageCode];
//    NSArray *results = [DataManager getFromEntity:kSurveyEntity withPredicate:predicate andSortBy:Nil ascending:YES];
//    
//    return results.firstObject;
//}

- (Survey *)getSurveyWithLanguageCode:(NSString *)languageCode
{
    NSNumber *surveyId = [[CompanyDataAccess sharedManager] getCurrentSurvey];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"surveyId == %@ && languageCode == %@", surveyId, languageCode];
    NSArray *results = [DataManager getFromEntity:kSurveyEntity withPredicate:predicate andSortBy:nil ascending:YES];
    
    if (!results)
    {
        predicate = [NSPredicate predicateWithFormat:@"surveyId == %@ && languageCode == %@", surveyId, @"es"];
        results = [DataManager getFromEntity:kSurveyEntity withPredicate:predicate andSortBy:nil ascending:YES];
    }
    
    return results.firstObject;
}

- (NSArray *)getSurveysWithLanguageCode:(NSString *)languageCode
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"languageCode == %@", languageCode];
    NSArray *results = [DataManager getFromEntity:kSurveyEntity withPredicate:predicate andSortBy:nil ascending:YES];
    
    return results;
}

- (NSArray *)getQuestionsForCurrentLanguage
{
    NSNumber *surveyId = [[CompanyDataAccess sharedManager] getCurrentSurvey];
   
//    NSString *languageCode = [LanguageDataAccess getLanguageCodeForLanguageId:[NSNumber numberWithInteger:self.languageId]];
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"survey.surveyId == %@ and languageCode == %@", surveyId, languageCode];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"survey.surveyId == %@ and languageCode == %@", surveyId, self.languageCode];

    //NSArray *results = [DataManager getFromEntity:kQuestionEntity withPredicate:predicate andSortBy:@"questionId" ascending:YES];
    NSArray *results = [DataManager getFromEntity:kQuestionEntity withPredicate:predicate andSortBy:@"sequence" ascending:YES];

    
    return results;
}

- (NSArray *)getQuestionsForDeviceLanguage
{
    NSNumber *surveyId = [[CompanyDataAccess sharedManager] getCurrentSurvey];
    NSString *locale   = [[NSLocale preferredLanguages] firstObject];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"survey.surveyId == %@ and languageCode == %@", surveyId, locale];
    //NSArray *results       = [DataManager getFromEntity:kQuestionEntity withPredicate:predicate andSortBy:@"questionId" ascending:YES];
    NSArray *results       = [DataManager getFromEntity:kQuestionEntity withPredicate:predicate andSortBy:@"sequence" ascending:YES];
    
    return results;
}

- (NSArray *)getAllQuestions
{
    NSNumber *surveyId = [[CompanyDataAccess sharedManager] getCurrentSurvey];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"survey.surveyId == %@", surveyId];
    //NSArray *results = [DataManager getFromEntity:kQuestionEntity withPredicate:predicate andSortBy:@"questionId" ascending:YES];
    NSArray *results = [DataManager getFromEntity:kQuestionEntity withPredicate:predicate andSortBy:@"sequence" ascending:YES];
    
    return results;
}

- (NSInteger)getNumberOfQuestionsWithLanguageCode:(NSString *)languageCode
{
    NSArray *results = [self getQuestionsForCurrentLanguage];
    
    return results.count;
}

- (void)setSurveys:(NSDictionary *)surveysDict forCompany:(Company *)company
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"company.idCompany == %@", company.idCompany];
    [DataManager removeFromEntity:kSurveyEntity withPredicate:predicate];
    
    NSArray *surveys = surveysDict[kResponseKeyResponseObject];
    
    [DataManager setSurveys:surveys forCompany:company];
}

- (void)setQuestions:(NSDictionary *)responseDict forSurvey:(Survey *)survey
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"survey.surveyId == %@", survey.surveyId];
    [DataManager removeFromEntity:kQuestionEntity withPredicate:predicate];
    
    NSArray *questions = [[responseDict[kResponseKeyResponseObject] firstObject] objectForKey:@"questions"];
    
    [DataManager setQuestions:questions forSurvey:survey];
}


@end

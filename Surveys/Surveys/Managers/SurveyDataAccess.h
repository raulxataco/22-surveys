//
//  SurveyDataAccess.h
//  Surveys
//
//  Created by Raúl Blánquez on 01/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Survey.h"
#import "Question.h"

@interface SurveyDataAccess : NSObject

@property (nonatomic, strong) NSString *languageCode;

+ (SurveyDataAccess *)sharedManager;

- (NSArray *)getSurveys;
- (Survey *)getFirstSurvey;
//- (Survey *)getSurveyWithLanguageId:(NSNumber *)languageId;
- (Survey *)getSurveyWithLanguageCode:(NSString *)languageCode;
- (NSArray *)getSurveysWithLanguageCode:(NSString *)languageCode;

- (NSArray *)getQuestionsForCurrentLanguage;
- (NSArray *)getQuestionsForDeviceLanguage;
- (NSArray *)getAllQuestions;
- (NSInteger)getNumberOfQuestionsWithLanguageCode:(NSString *)languageCode;

- (void)setSurveys:(NSDictionary *)surveysDict forCompany:(Company *)company;
- (void)setQuestions:(NSDictionary *)questionsDict forSurvey:(Survey *)survey;

@end

//
//  ServerDataAccess.h
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Server.h"

@interface ServerDataAccess : NSObject

+ (ServerDataAccess *)sharedManager;
+ (Server *)getServerWithId:(NSNumber *)serverId;

- (NSArray *)getServersForCompany:(NSNumber *)companyId;
- (void)setServers:(NSDictionary *)serversDict forCompany:(Company *)company;

@end

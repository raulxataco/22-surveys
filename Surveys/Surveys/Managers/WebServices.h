//
//  WebServices.h
//  Surveys
//
//  Created by Raúl Blánquez on 2/11/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kResponseKeySuccess                     @"success"
#define kResponseKeyResponseObject              @"response"
#define kRequestHeaderNameContentType           @"Content-Type"
#define kRequestHeaderValueContentTypeJSON      @"application/json"
#define kRequestHeaderValueContentTypeTextPlain @"text/plain"
#define kRequestHeaderValueCharsetUTF8          @"charset = UTF-8"

#define kPathKeyGetData                         @"pathGetData"
#define kPathKeySetData                         @"pathSetData"

#define kWSNotificationEntityInfo               @"wsEntityInfo"
#define kWSNotificationServersInfo              @"wsServersInfo"
#define kWSNotificationSurveysInfo              @"wsSurveysInfo"
#define kWSNotificationQuestionsInfo            @"wsQuestionsInfo"
#define kWSNotificationHistory                  @"wsHistory"

#define kWSAction                               @"action"
#define kWSActionEntity                         @"entity"
#define kWSActionPeople                         @"people"
#define kWSActionSurveys                        @"surveys"
#define kWSActionSurvey                         @"survey"
#define kWSActionQuestions                      @"questions"
#define kWSActionSend                           @"send"
#define kWSEntityId                             @"id"
#define kWSSurveyId                             @"id"
#define kWSLanguageCode                         @"lang"
#define kWSEmailId                              @"email"

@interface WebServices : NSObject

+ (NSMutableDictionary *)defaultRequestHeaders;

+ (void)makeGetRequestToURL:(NSString *)urlString
             withParameters:(NSDictionary *)parameters
                   withBody:(NSData *)body
         withNotificationId:(NSString *)notificationId;
+ (void)makePostRequestToURL:(NSString *)urlString
              withParameters:(NSDictionary *)parameters
                    withBody:(NSData *)body
          withNotificationId:(NSString *)notificationId;

+ (NSString *)stringUrlForPathKey:(NSString *)pathKey;
+ (NSString *)stringUrlForPathString:(NSString *)pathString;

@end

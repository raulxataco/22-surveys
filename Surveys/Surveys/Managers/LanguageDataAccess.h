//
//  LanguageDataAccess.h
//  Surveys
//
//  Created by Raúl Blánquez on 02/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Language.h"

@interface LanguageDataAccess : NSObject

+ (NSArray *)getLanguages;
+ (Language *)getLanguage:(NSString *)languageCode;
+ (NSString *)getLanguageCodeForLanguageId:(NSNumber *)languageId;
+ (NSNumber *)getLanguageIdForLanguageCode:(NSString *)languageCode;

@end

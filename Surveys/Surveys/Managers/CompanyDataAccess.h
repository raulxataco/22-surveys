//
//  CompanyDataAccess.h
//  Surveys
//
//  Created by Raúl Blánquez on 29/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Company.h"

@interface CompanyDataAccess : NSObject

+ (CompanyDataAccess *)sharedManager;

- (Company *)getCompany;
- (Company *)setCompany:(NSDictionary *)companyDict;
- (NSNumber *)getCurrentSurvey;
- (NSString *)getWellcomeMessageForLanguage:(NSString *)languageCode;
- (NSString *)getThanksMessageForLanguage:(NSString *)languageCode;

@end

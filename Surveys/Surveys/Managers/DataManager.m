//
//  DataManager.m
//  Surveys
//
//  Created by Raúl Blánquez on 29/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "DataManager.h"
#import "AppDelegate.h"
#import "GlobalHeader.h"
#import "Server.h"
#import "Table.h"
#import "Survey.h"
#import "AnswerFormat1.h"
#import "AnswerFormat2.h"
#import "AnswerFormat3.h"
#import "Question.h"
#import "Language.h"
#import "CompanyDataAccess.h"
#import "SurveyDataAccess.h"
#import "History.h"
#import "Comments.h"
#import "DeviceUtils.h"
#import "Message.h"

@implementation DataManager

// *****************************************************************************
#pragma mark -                                          Get operations
// *****************************************************************************
+ (NSArray *)getFromEntity:(NSString *)entityName
             withPredicate:(NSPredicate *)predicate
                 andSortBy:(NSString *)sortKey
                 ascending:(BOOL)ascending
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    //Filter with predicate
    if (predicate)
    {
        [fetchRequest setPredicate:predicate];
    }
    
    // Sort by key
    if (sortKey)
    {
        NSSortDescriptor *keySort = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:ascending selector:@selector(localizedCaseInsensitiveCompare:)];
        fetchRequest.sortDescriptors = [NSArray arrayWithObject:keySort];
    }
    
    NSError * error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    return fetchedObjects;
}

+ (NSInteger)countFromEntity:(NSString *)entityName withPredicate:(NSPredicate *)predicate
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    //Filter with predicate
    if (predicate)
    {
        [fetchRequest setPredicate:predicate];
    }
    
    NSInteger result = [context countForFetchRequest:fetchRequest error:nil];
    
    if (result == NSNotFound)
    {
        return 0;
    }
    else
    {
        return result;
    }
}

// *****************************************************************************
#pragma mark -                                          Creation operations
// *****************************************************************************
+ (void)createCompany:(NSDictionary *)companyDict
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    [self removeFromEntity:kCompanyEntity withPredicate:Nil];

    Company *companyEntity = (Company *)[NSEntityDescription insertNewObjectForEntityForName:kCompanyEntity inManagedObjectContext:context];
    
    companyEntity.idCompany = companyDict[@"companyId"];
    companyEntity.name      = companyDict[@"name"];
    companyEntity.customers = companyDict[@"customersNumber"];
    
    [self saveContext];
}

+ (void)createServers:(NSArray *)servers
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    [self removeFromEntity:kServerEntity withPredicate:Nil];
    
    Company *company = [[CompanyDataAccess sharedManager] getCompany];
    if (company)
    {
        for (NSDictionary *item in servers)
        {
            Server *server = (Server *)[NSEntityDescription insertNewObjectForEntityForName:kServerEntity inManagedObjectContext:context];
            
            server.serverId = item[@"serverId"];
            server.name     = item[@"name"];
            
            NSData *imageData = UIImagePNGRepresentation([UIImage imageNamed:@"profile_noimage.png"]);
            server.photo    = imageData;
            
            server.company  = company;
            
            [company addServersObject:server];
        }
    }
}

+ (void)createTables:(NSArray *)tableList
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    [self removeFromEntity:kTableEntity withPredicate:Nil];
    
    Company *company = [[CompanyDataAccess sharedManager] getCompany];
    if (company)
    {
        for (NSString *item in tableList)
        {
            Table *table = (Table *)[NSEntityDescription insertNewObjectForEntityForName:kTableEntity inManagedObjectContext:context];
            
            table.tableId = item;
            table.company  = company;
            
            [company addTablesObject:table];
        }
    }
}

//+ (void)createLanguages:(NSArray *)languagesList
//{
//    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
//    
//    [self removeFromEntity:kLanguageEntity withPredicate:Nil];
//    
//    for (NSDictionary *item in languagesList)
//    {
//        Language *language = (Language *)[NSEntityDescription insertNewObjectForEntityForName:kLanguageEntity inManagedObjectContext:context];
//        
//        language.languageId   = item[@"languageId"];
//        language.languageCode = item[@"languageCode"];
//        language.name         = item[@"name"];
//    }
//}

+ (void)createSurveys:(NSArray *)surveyList
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    [self removeFromEntity:kSurveyEntity withPredicate:Nil];
    
    Company *company = [[CompanyDataAccess sharedManager] getCompany];
    if (company)
    {
        for (NSDictionary *surveyDict in surveyList)
        {
            NSArray *languages = surveyDict[@"languages"];
            
            for (NSDictionary *language in languages)
            {
                Survey *surveyEntity = (Survey *)[NSEntityDescription insertNewObjectForEntityForName:kSurveyEntity inManagedObjectContext:context];
                
                surveyEntity.surveyId     = surveyDict[@"surveyId"];
                surveyEntity.title        = language[@"title"];
                surveyEntity.info         = language[@"info"];
                surveyEntity.languageCode = language[@"languageCode"];
                surveyEntity.company      = company;
                
                [company addSurveysObject:surveyEntity];
            }
        }
    }
}

// *****************************************************************************
#pragma mark -                                          Delete operations
// *****************************************************************************
+ (void)removeFromEntity:(NSString *)entityName withPredicate:(NSPredicate *)predicate
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    if(predicate != nil)
    {
        [request setPredicate:predicate];
    }
    
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    
    NSError * error = nil;
    NSArray * elements = [context executeFetchRequest:request error:&error];
    
    for (NSManagedObject *object in elements)
    {
        [context deleteObject:object];
    }
}

// *****************************************************************************
#pragma mark -                                          Update operations
// *****************************************************************************
+ (Company *)setCompany:(Company *)company
              idCompany:(NSNumber *)idCompany
                   name:(NSString *)name
                   mail:(NSString *)email
                  phone:(NSString *)phone
                address:(NSString *)address
                   city:(NSString *)city
              customers:(NSNumber *)customers
                 tables:(NSNumber *)tables
               messages:(NSArray *)messages
              languages:(NSArray *)languages
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];

    if (!company)
    {
        company = (Company *)[NSEntityDescription insertNewObjectForEntityForName:kCompanyEntity
                                                           inManagedObjectContext:context];
    }
    
    company.idCompany = idCompany;
    company.name      = name;
    company.customers = customers;
    company.phone     = phone;
    company.address   = address;
    company.city      = city;
    company.email     = email;

    [self removeFromEntity:kTableEntity withPredicate:Nil];
    
    if (company)
    {
        for (NSInteger i = 1; i <= [tables integerValue]; i++)
        {
            Table *table = (Table *)[NSEntityDescription insertNewObjectForEntityForName:kTableEntity inManagedObjectContext:context];
            
            table.tableId = [NSString stringWithFormat:@"%d", (int)i];
            table.company  = company;
            
            [company addTablesObject:table];
        }
    }
    
    if (company)
    {
        [self removeFromEntity:kMessageEntity withPredicate:Nil];

        for (NSDictionary *item in messages)
        {
            Message *message = (Message *)[NSEntityDescription insertNewObjectForEntityForName:kMessageEntity inManagedObjectContext:context];
            
            message.languageCode = item[@"lang"] ? item[@"lang"] : @"es";
            message.welcome      = item[@"welcome"] ? item[@"welcome"] : @"";
            message.thanks       = item[@"thanks"] ? item[@"thanks"] : @"";
            message.company      = company;
            
            [company addMessagesObject:message];
        }
    }
    
    if (company)
    {
        [self removeFromEntity:kLanguageEntity withPredicate:Nil];
        
        NSInteger index = 0;
        for (NSDictionary *item in languages)
        {
            Language *language = (Language *)[NSEntityDescription insertNewObjectForEntityForName:kLanguageEntity inManagedObjectContext:context];
            
            language.languageId   = @(index);
            language.languageCode = item[@"lang"];
            language.name         = item[@"name"];
            language.native       = item[@"native"];
            index++;
        }
    }

    [self saveContext];
    
    return company;
}

+ (void)setServers:(NSArray *)servers forCompany:(Company *)company
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];

    for (NSDictionary *serverDict in servers)
    {
        Server *serverEntity = (Server *)[NSEntityDescription insertNewObjectForEntityForName:kServerEntity
                                                                       inManagedObjectContext:context];

        NSNumber *serverId    = serverDict[@"person"] ? @([serverDict[@"person"] integerValue]) : @0;
        NSString *name        = serverDict[@"name"] ? serverDict[@"name"] : @"";

        serverEntity.serverId = serverId;
        serverEntity.name     = name;
        serverEntity.photo    = UIImagePNGRepresentation([UIImage imageNamed:@"profile_noimage.png"]);

        serverEntity.company  = company;
        
        [company addServersObject:serverEntity];
    }
    
    [self saveContext];
}

+ (void)setSurveys:(NSArray *)surveys forCompany:(Company *)company
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    for (NSDictionary *surveyDict in surveys)
    {
        Survey *surveyEntity = (Survey *)[NSEntityDescription insertNewObjectForEntityForName:kSurveyEntity
                                                                       inManagedObjectContext:context];
        
        NSNumber *surveyId        = surveyDict[@"survey"] ? @([surveyDict[@"survey"] integerValue]) : @0;
        NSString *name            = surveyDict[@"description"] ? surveyDict[@"description"] : NSLocalizedString(@"survey_name", nil);
        NSString *info            = surveyDict[@"date"] ? surveyDict[@"date"] : @"";
        NSString *language        = surveyDict[@"language"] ? surveyDict[@"language"] : @"es";

        surveyEntity.surveyId     = surveyId;
        surveyEntity.title        = name;
        surveyEntity.info         = info;
        surveyEntity.languageCode = language;
        
        surveyEntity.company  = company;
        
        [company addSurveysObject:surveyEntity];
        if ([company.currentSurveyId isEqualToNumber:@0])
        {
            company.currentSurveyId = surveyEntity.surveyId;
        }
    }
    
    [self saveContext];
}

+ (void)setQuestions:(NSArray *)questions forSurvey:(Survey *)survey
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    NSInteger index = 1;
    
    for (NSDictionary *questionDict in questions)
    {
        Question *questionEntity = (Question *)[NSEntityDescription insertNewObjectForEntityForName:kQuestionEntity
                                                                             inManagedObjectContext:context];
        
        NSNumber *format            = questionDict[@"format"] ? @([questionDict[@"format"] integerValue]) : @1;
        NSString *language          = questionDict[@"language"] ? questionDict[@"language"] : @"es";
        NSString *question          = questionDict[@"question"] ? questionDict[@"question"] : @"Pregunta vacía";
        NSArray *options            = questionDict[@"options"];
        NSNumber *questionId        = questionDict[@"id"] ? @([questionDict[@"id"] integerValue]) : @(index++);
        NSNumber *sequence          = questionDict[@"sequence"] ? @([questionDict[@"sequence"] integerValue]) : @1;

        questionEntity.format       = format;
        questionEntity.languageCode = language;
        questionEntity.question     = question;
        questionEntity.questionId   = questionId;
        questionEntity.sequence     = sequence;

        questionEntity.survey       = survey;
        
        [survey addQuestionsObject:questionEntity];
        
        ServerQuestionType type = (ServerQuestionType)[format integerValue];
        
        switch (type) {
            case kQuestion5Answers:
                if (options.count == 5)
                {
                    AnswerFormat1 *answerEntity = (AnswerFormat1 *)[NSEntityDescription insertNewObjectForEntityForName:kAnswer1Entity
                                                                                                 inManagedObjectContext:context];
                    NSDictionary *option = options[0];
                    answerEntity.option1 = option[@"text"] ? option[@"text"] : @"Respuesta indefinida";
                    option               = options[1];
                    answerEntity.option2 = option[@"text"] ? option[@"text"] : @"Respuesta indefinida";
                    option               = options[2];
                    answerEntity.option3 = option[@"text"] ? option[@"text"] : @"Respuesta indefinida";
                    option               = options[3];
                    answerEntity.option4 = option[@"text"] ? option[@"text"] : @"Respuesta indefinida";
                    option               = options[4];
                    answerEntity.option5 = option[@"text"] ? option[@"text"] : @"Respuesta indefinida";
                    
                    answerEntity.question= questionEntity;
                }
                break;
            case kQuestion3Answers:
                if (options.count == 3)
                {
                    AnswerFormat2 *answerEntity = (AnswerFormat2 *)[NSEntityDescription insertNewObjectForEntityForName:kAnswer2Entity
                                                                                                 inManagedObjectContext:context];
                    NSDictionary *option = options[0];
                    answerEntity.option1 = option[@"text"] ? option[@"text"] : @"Respuesta indefinida";
                    option               = options[1];
                    answerEntity.option2 = option[@"text"] ? option[@"text"] : @"Respuesta indefinida";
                    option               = options[2];
                    answerEntity.option3 = option[@"text"] ? option[@"text"] : @"Respuesta indefinida";
                    
                    answerEntity.question= questionEntity;
                }
                break;
            case kQuestion2Answers:
            {
                AnswerFormat3 *answerEntity = (AnswerFormat3 *)[NSEntityDescription insertNewObjectForEntityForName:kAnswer3Entity
                                                                                             inManagedObjectContext:context];
                if (options.count == 2)
                {
                    NSDictionary *option = options[0];
                    answerEntity.option1 = option[@"text"] ? option[@"text"] : @"Respuesta indefinida";
                    option               = options[1];
                    answerEntity.option2 = option[@"text"] ? option[@"text"] : @"Respuesta indefinida";
                }
                else
                {
                    answerEntity.option1 = @"yes";
                    answerEntity.option2 = @"no";
                }
                
                answerEntity.question= questionEntity;

                break;
            }
            default:
                break;
        }
    }
    
    [self saveContext];
}

+ (void)setHistoryForCompany:(Company *)company
                   andServer:(Server *)server
                    andTable:(Table *)table
                withQuestion:(Question *)question
                    andValue:(NSNumber *)answer
                      inDate:(NSDate *)date
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    History *history = (History *)[NSEntityDescription insertNewObjectForEntityForName:KHistoryEntity
                                                                inManagedObjectContext:context];
    
    history.company  = company;
    history.server   = server;
    history.table    = table;
    history.question = question;
    history.answer   = answer;
    history.date     = date;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddhhmmss"];
    
    NSString *zuluDate = [formatter stringFromDate:date];
    
    history.completedSurveyId = [NSString stringWithFormat:@"%@#%@", [DeviceUtils deviceID], zuluDate];
    
    [self saveContext];
}

+ (void)setHistoryForCompany:(Company *)company
                   andServer:(Server *)server
                    andTable:(Table *)table
                withQuestion:(Question *)question
                  andComment:(NSString *)comment
                      inDate:(NSDate *)date
{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    Comments *commentEntity = (Comments *)[NSEntityDescription insertNewObjectForEntityForName:kCommentEntity
                                                                        inManagedObjectContext:context];
    
    commentEntity.company  = company;
    commentEntity.server   = server;
    commentEntity.table    = table;
    commentEntity.question = question;
    commentEntity.answer   = comment;
    commentEntity.date     = date;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddhhmmss"];
    
    NSString *zuluDate = [formatter stringFromDate:date];
    
    commentEntity.completedSurveyId = [NSString stringWithFormat:@"%@#%@", [DeviceUtils deviceID], zuluDate];
    
    [self saveContext];
}

// *****************************************************************************
#pragma mark -                                          Helper Methods
// *****************************************************************************
+ (void)saveContext
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}


@end

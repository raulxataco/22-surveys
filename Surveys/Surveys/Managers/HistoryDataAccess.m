//
//  HistoryDataAccess.m
//  Surveys
//
//  Created by Raúl Blánquez on 16/11/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "HistoryDataAccess.h"
#import "Question.h"
#import "DataManager.h"
#import "CompanyDataAccess.h"
#import "GlobalHeader.h"
#import "History.h"

@implementation HistoryDataAccess

// *****************************************************************************
#pragma mark -                                          Singleton stuff
// *****************************************************************************
+ (HistoryDataAccess *)sharedManager
{
    static HistoryDataAccess *sharedObject = nil;
    static dispatch_once_t singletonPredicate;
    
    dispatch_once(&singletonPredicate, ^{
        sharedObject = [[super allocWithZone:nil] init];
    });
    
    return sharedObject;
}

// *****************************************************************************
#pragma mark -                                          Public methods
// *****************************************************************************
- (void)setHistoryForQuestion:(Question *)question andANswer:(NSNumber *)answer
{
    Company *company = [[CompanyDataAccess sharedManager] getCompany];
#warning Pendiente incluir camarero y mesa
    Server *server   = Nil;
    Table *table     = Nil;
    
    [DataManager setHistoryForCompany:company
                            andServer:server
                             andTable:table
                         withQuestion:question
                             andValue:answer
                               inDate:[NSDate date]];
}

- (void)setHistoryForQuestion:(Question *)question andComment:(NSString *)comment
{
    if (comment.length == 0)
    {
        return;
    }
    
    Company *company = [[CompanyDataAccess sharedManager] getCompany];
    Server *server   = Nil;
    Table *table     = Nil;
    
    [DataManager setHistoryForCompany:company
                            andServer:server
                             andTable:table
                         withQuestion:question
                             andComment:comment
                               inDate:[NSDate date]];
}

- (NSArray *)getHistory
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"question.survey.surveyId == %@", [[CompanyDataAccess sharedManager] getCurrentSurvey]];
    NSArray *results = [DataManager getFromEntity:KHistoryEntity withPredicate:predicate andSortBy:nil ascending:YES];
    
    return results;
}

- (BOOL)historyNotUploadedToServer
{
    NSArray *results = [DataManager getFromEntity:KHistoryEntity withPredicate:Nil andSortBy:Nil ascending:YES];
    
    return results.count > 0;
}

- (NSDictionary *)getHistoryForQuestion:(NSNumber *)questionId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"question.survey.surveyId == %@ and question.questionId == %@", [[CompanyDataAccess sharedManager] getCurrentSurvey], questionId];
    NSArray *results = [DataManager getFromEntity:KHistoryEntity withPredicate:predicate andSortBy:nil ascending:YES];
    
    NSDictionary *dictResults = [self entitiesToJson:results];

    return dictResults;
}

- (NSDictionary *)getCommentsForQuestion:(NSNumber *)questionId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"question.survey.surveyId == %@ and question.questionId == %@", [[CompanyDataAccess sharedManager] getCurrentSurvey], questionId];
    NSArray *results = [DataManager getFromEntity:kCommentEntity withPredicate:predicate andSortBy:nil ascending:YES];
    
    NSDictionary *dictResults = [self entitiesToJson:results];
    
    return dictResults;
}

- (NSDictionary *)getHistoryRange
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"question.survey.surveyId == %@", [[CompanyDataAccess sharedManager] getCurrentSurvey]];
    NSArray *results = [DataManager getFromEntity:KHistoryEntity withPredicate:predicate andSortBy:@"date" ascending:YES];
    
    History *first = results.firstObject;
    History *last  = results.lastObject;

    if (first && last)
    {
        return @{ @"start" : first.date, @"end" : last.date};
    }
    else
    {
        return Nil;
    }
}

- (NSInteger)getNumberOfAnswersForQuestion:(NSNumber *)questionId andSurveyId:(NSNumber *)surveyId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"question.questionId == %@ and question.survey.surveyId == %@", questionId, surveyId];
    return [DataManager countFromEntity:KHistoryEntity withPredicate:predicate];
}

- (NSInteger)getNumberOfAnswersForQuestion:(NSNumber *)questionId andOption:(NSNumber *)option andSurveyId:(NSNumber *)surveyId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"question.questionId == %@ and answer == %@ and question.survey.surveyId == %@", questionId, option, surveyId];
    
    return [DataManager countFromEntity:KHistoryEntity withPredicate:predicate];
}

- (void)removeHistoryForSurveyId:(NSNumber *)surveyId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"question.survey.surveyId == %@", surveyId];
    
    [DataManager removeFromEntity:KHistoryEntity withPredicate:predicate];
    [DataManager removeFromEntity:kCommentEntity withPredicate:predicate];
    
    [DataManager saveContext];
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (NSDictionary *)entitiesToJson:(NSArray *)arrayOfEntities
{
    NSMutableArray *arrayOfDict = [[NSMutableArray alloc] initWithCapacity:arrayOfEntities.count];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yy"];
    
    for (History *item in arrayOfEntities)
    {
        
        NSDictionary *newItem = @{@"surveyId" : item.completedSurveyId,
                                  @"date"     : [formatter stringFromDate:item.date],
                                  @"answer"   : [NSString stringWithFormat:@"%@", item.answer]};
        [arrayOfDict addObject:newItem];
    }
    
    History *fistItem = [arrayOfEntities firstObject];
    
    return @{ @"question" : [NSString stringWithFormat:@"%@", fistItem.question.questionId],
              @"answers"  : arrayOfDict};
}

@end

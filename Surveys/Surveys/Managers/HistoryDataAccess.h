//
//  HistoryDataAccess.h
//  Surveys
//
//  Created by Raúl Blánquez on 16/11/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"

@interface HistoryDataAccess : NSObject

+ (HistoryDataAccess *)sharedManager;

- (void)setHistoryForQuestion:(Question *)question andANswer:(NSNumber *)answer;
- (void)setHistoryForQuestion:(Question *)question andComment:(NSString *)comment;

- (NSInteger)getNumberOfAnswersForQuestion:(NSNumber *)questionId andSurveyId:(NSNumber *)surveyId;
- (NSInteger)getNumberOfAnswersForQuestion:(NSNumber *)questionId andOption:(NSNumber *)option andSurveyId:(NSNumber *)surveyId;

- (NSArray *)getHistory;
- (BOOL)historyNotUploadedToServer;
- (NSDictionary *)getHistoryForQuestion:(NSNumber *)questionId;
- (NSDictionary *)getCommentsForQuestion:(NSNumber *)questionId;
- (NSDictionary *)getHistoryRange;

- (void)removeHistoryForSurveyId:(NSNumber *)surveyId;

@end

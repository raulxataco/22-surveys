//
//  ServerDataAccess.m
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "ServerDataAccess.h"
#import "WebServices.h"
#import "DataManager.h"
#import "GlobalHeader.h"

@implementation ServerDataAccess

// *****************************************************************************
#pragma mark -                                          Singleton stuff
// *****************************************************************************
+ (ServerDataAccess *)sharedManager
{
    static ServerDataAccess *sharedObject = nil;
    static dispatch_once_t singletonPredicate;
    
    dispatch_once(&singletonPredicate, ^{
        sharedObject = [[super allocWithZone:nil] init];
    });
    
    return sharedObject;
}

// *****************************************************************************
#pragma mark -                                          Public methods
// *****************************************************************************
- (NSArray *)getServersForCompany:(NSNumber *)companyId
{
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"company == %@", companyId];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"company.idCompany == %@", companyId];

    NSArray *results = [DataManager getFromEntity:kServerEntity withPredicate:predicate andSortBy:Nil ascending:YES];
    
    return results;
}

- (void)setServers:(NSDictionary *)serversDict forCompany:(Company *)company
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"company.idCompany == %@", company.idCompany];
    [DataManager removeFromEntity:kServerEntity withPredicate:predicate];
    
    NSArray *servers = serversDict[kResponseKeyResponseObject];
    
    [DataManager setServers:servers forCompany:company];    
}

+ (Server *)getServerWithId:(NSNumber *)serverId
{
    NSArray *results = [DataManager getFromEntity:kServerEntity
                                    withPredicate:[NSPredicate predicateWithFormat:@"serverId == %@", serverId]
                                        andSortBy:Nil
                                        ascending:YES];
    return results.firstObject;
}

@end

//
//  DataManager.h
//  Surveys
//
//  Created by Raúl Blánquez on 29/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Company.h"
#import "Question.h"

@interface DataManager : NSObject

//General
+ (NSArray *)getFromEntity:(NSString *)entityName
             withPredicate:(NSPredicate *)predicate
                 andSortBy:(NSString *)sortKey
                 ascending:(BOOL)ascending;
+ (void)removeFromEntity:(NSString *)entityName withPredicate:(NSPredicate *)predicate;
+ (NSInteger)countFromEntity:(NSString *)entityName
          withPredicate:(NSPredicate *)predicate;
+ (void)saveContext;

//Company
+ (void)createCompany:(NSDictionary *)companyDict;
+ (Company *)setCompany:(Company *)company
              idCompany:(NSNumber *)idCompany
                   name:(NSString *)name
                   mail:(NSString *)email
                  phone:(NSString *)phone
                address:(NSString *)address
                   city:(NSString *)city
              customers:(NSNumber *)customers
                 tables:(NSNumber *)tables
               messages:(NSArray *)messages
              languages:(NSArray *)languages;

//Servers
+ (void)createServers:(NSArray *)serverList;
+ (void)setServers:(NSArray *)servers forCompany:(Company *)company;

//Surveys
+ (void)setSurveys:(NSArray *)surveys forCompany:(Company *)company;
+ (void)setQuestions:(NSArray *)questions forSurvey:(Survey *)survey;

+ (void)createTables:(NSArray *)tableList;
//+ (void)createLanguages:(NSArray *)languageList;

//History
+ (void)setHistoryForCompany:(Company *)company
                   andServer:(Server *)server
                    andTable:(Table *)table
                withQuestion:(Question *)question
                    andValue:(NSNumber *)answer
                      inDate:(NSDate *)date;
+ (void)setHistoryForCompany:(Company *)company
                   andServer:(Server *)server
                    andTable:(Table *)table
                withQuestion:(Question *)question
                  andComment:(NSString *)comment
                      inDate:(NSDate *)date;

@end

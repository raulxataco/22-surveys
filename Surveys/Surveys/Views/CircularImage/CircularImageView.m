//
//  CircularImageView.m
//  DigitalHealth
//
//  Created by Daniel Ruiz on 07/05/14.
//  Copyright (c) 2014 Axa. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "CircularImageView.h"

@interface CircularImageView()

@property (strong, nonatomic) UIImageView *mainImageView;

@end

@implementation CircularImageView

#pragma mark - View Lifecycle

- (id)init
{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews
{
    // Init subviews
    self.mainImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.mainImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.mainImageView.clipsToBounds = YES;
    
    self.backgroundColor = nil;
    
    [self addSubview:self.mainImageView];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.mainImageView.frame = [self _mainImageViewFrame];
    
    [self prepareRoundedView];
}

#pragma mark - Custom getters and setters

- (void)setImage:(UIImage *)image
{
    self.mainImageView.image = image;
}

#pragma mark - Private methods

- (void)prepareRoundedView
{
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat diameter = width;
    if (height < width) {
        diameter = height;
    }
    CGFloat cornerRadius = diameter / 2.0f;
    
    self.layer.cornerRadius = cornerRadius;
    self.mainImageView.layer.cornerRadius = cornerRadius;
}

- (CGRect)_mainImageViewFrame
{
    return self.bounds;
}

@end

//
//  CircularImageView.h
//  DigitalHealth
//
//  Created by Daniel Ruiz on 07/05/14.
//  Copyright (c) 2014 Axa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircularImageView : UIView

@property (weak, nonatomic) UIImage *image;

@end

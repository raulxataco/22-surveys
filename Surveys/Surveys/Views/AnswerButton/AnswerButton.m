//
//  AnswerButton.m
//  Surveys
//
//  Created by Raúl Blánquez on 15/11/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "AnswerButton.h"
@import QuartzCore;

@implementation AnswerButton

-(void) setSelected:(BOOL)selected
{
    if (selected)
    {
        self.backgroundColor   = [UIColor darkGrayColor];
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius= 5;
    }
    else
    {
        self.backgroundColor = [UIColor clearColor];
        self.layer.borderWidth = 0;
    }
    
    [super setSelected:selected];
}

@end

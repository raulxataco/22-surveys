//
//  Question.m
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "Question.h"
#import "AnswerFormat1.h"
#import "AnswerFormat2.h"
#import "AnswerFormat3.h"
#import "Comments.h"
#import "History.h"
#import "Survey.h"


@implementation Question

@dynamic format;
@dynamic languageCode;
@dynamic question;
@dynamic questionId;
@dynamic sequence;
@dynamic answerFormat1;
@dynamic answerFormat2;
@dynamic answerFormat3;
@dynamic comments;
@dynamic history;
@dynamic survey;

@end

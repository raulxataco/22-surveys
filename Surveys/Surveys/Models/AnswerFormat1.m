//
//  AnswerFormat1.m
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "AnswerFormat1.h"
#import "Question.h"


@implementation AnswerFormat1

@dynamic option1;
@dynamic option2;
@dynamic option3;
@dynamic option4;
@dynamic option5;
@dynamic question;

@end

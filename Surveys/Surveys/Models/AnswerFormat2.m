//
//  AnswerFormat2.m
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "AnswerFormat2.h"
#import "Question.h"


@implementation AnswerFormat2

@dynamic option1;
@dynamic option2;
@dynamic option3;
@dynamic question;

@end

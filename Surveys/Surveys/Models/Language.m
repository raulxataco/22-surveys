//
//  Language.m
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "Language.h"


@implementation Language

@dynamic languageCode;
@dynamic languageId;
@dynamic name;
@dynamic native;

@end

//
//  AnswerFormat3.m
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "AnswerFormat3.h"
#import "Question.h"


@implementation AnswerFormat3

@dynamic option1;
@dynamic option2;
@dynamic question;

@end

//
//  Company.h
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Comments, History, Message, Server, Survey, Table;

@interface Company : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSData * backgroundImage;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSNumber * currentSurveyId;
@property (nonatomic, retain) NSNumber * customers;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSNumber * idCompany;
@property (nonatomic, retain) NSData * logo;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSSet *comments;
@property (nonatomic, retain) NSSet *history;
@property (nonatomic, retain) NSSet *messages;
@property (nonatomic, retain) NSSet *servers;
@property (nonatomic, retain) NSSet *surveys;
@property (nonatomic, retain) NSSet *tables;
@end

@interface Company (CoreDataGeneratedAccessors)

- (void)addCommentsObject:(Comments *)value;
- (void)removeCommentsObject:(Comments *)value;
- (void)addComments:(NSSet *)values;
- (void)removeComments:(NSSet *)values;

- (void)addHistoryObject:(History *)value;
- (void)removeHistoryObject:(History *)value;
- (void)addHistory:(NSSet *)values;
- (void)removeHistory:(NSSet *)values;

- (void)addMessagesObject:(Message *)value;
- (void)removeMessagesObject:(Message *)value;
- (void)addMessages:(NSSet *)values;
- (void)removeMessages:(NSSet *)values;

- (void)addServersObject:(Server *)value;
- (void)removeServersObject:(Server *)value;
- (void)addServers:(NSSet *)values;
- (void)removeServers:(NSSet *)values;

- (void)addSurveysObject:(Survey *)value;
- (void)removeSurveysObject:(Survey *)value;
- (void)addSurveys:(NSSet *)values;
- (void)removeSurveys:(NSSet *)values;

- (void)addTablesObject:(Table *)value;
- (void)removeTablesObject:(Table *)value;
- (void)addTables:(NSSet *)values;
- (void)removeTables:(NSSet *)values;

@end

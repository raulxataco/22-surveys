//
//  Message.m
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "Message.h"
#import "Company.h"


@implementation Message

@dynamic languageCode;
@dynamic thanks;
@dynamic welcome;
@dynamic company;

@end

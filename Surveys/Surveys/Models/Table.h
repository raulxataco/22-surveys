//
//  Table.h
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Comments, Company, History;

@interface Table : NSManagedObject

@property (nonatomic, retain) NSString * tableId;
@property (nonatomic, retain) Comments *comments;
@property (nonatomic, retain) Company *company;
@property (nonatomic, retain) NSSet *history;
@end

@interface Table (CoreDataGeneratedAccessors)

- (void)addHistoryObject:(History *)value;
- (void)removeHistoryObject:(History *)value;
- (void)addHistory:(NSSet *)values;
- (void)removeHistory:(NSSet *)values;

@end

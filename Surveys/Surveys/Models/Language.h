//
//  Language.h
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Language : NSManagedObject

@property (nonatomic, retain) NSString * languageCode;
@property (nonatomic, retain) NSNumber * languageId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * native;

@end

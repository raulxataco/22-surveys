//
//  Comments.m
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "Comments.h"
#import "Company.h"
#import "Question.h"
#import "Server.h"
#import "Table.h"


@implementation Comments

@dynamic answer;
@dynamic completedSurveyId;
@dynamic date;
@dynamic company;
@dynamic question;
@dynamic server;
@dynamic table;

@end

//
//  Message.h
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Company;

@interface Message : NSManagedObject

@property (nonatomic, retain) NSString * languageCode;
@property (nonatomic, retain) NSString * thanks;
@property (nonatomic, retain) NSString * welcome;
@property (nonatomic, retain) Company *company;

@end

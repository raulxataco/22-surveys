//
//  AnswerFormat1.h
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Question;

@interface AnswerFormat1 : NSManagedObject

@property (nonatomic, retain) NSString * option1;
@property (nonatomic, retain) NSString * option2;
@property (nonatomic, retain) NSString * option3;
@property (nonatomic, retain) NSString * option4;
@property (nonatomic, retain) NSString * option5;
@property (nonatomic, retain) Question *question;

@end

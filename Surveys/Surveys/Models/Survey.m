//
//  Survey.m
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "Survey.h"
#import "Company.h"
#import "Question.h"


@implementation Survey

@dynamic info;
@dynamic languageCode;
@dynamic surveyId;
@dynamic title;
@dynamic company;
@dynamic questions;

@end

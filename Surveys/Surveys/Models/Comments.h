//
//  Comments.h
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Company, Question, Server, Table;

@interface Comments : NSManagedObject

@property (nonatomic, retain) NSString * answer;
@property (nonatomic, retain) NSString * completedSurveyId;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) Company *company;
@property (nonatomic, retain) Question *question;
@property (nonatomic, retain) Server *server;
@property (nonatomic, retain) Table *table;

@end

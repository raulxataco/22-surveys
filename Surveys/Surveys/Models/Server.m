//
//  Server.m
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "Server.h"
#import "Comments.h"
#import "Company.h"
#import "History.h"


@implementation Server

@dynamic name;
@dynamic photo;
@dynamic serverId;
@dynamic comments;
@dynamic company;
@dynamic history;

@end

//
//  Company.m
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "Company.h"
#import "Comments.h"
#import "History.h"
#import "Message.h"
#import "Server.h"
#import "Survey.h"
#import "Table.h"


@implementation Company

@dynamic address;
@dynamic backgroundImage;
@dynamic city;
@dynamic currentSurveyId;
@dynamic customers;
@dynamic email;
@dynamic idCompany;
@dynamic logo;
@dynamic name;
@dynamic phone;
@dynamic comments;
@dynamic history;
@dynamic messages;
@dynamic servers;
@dynamic surveys;
@dynamic tables;

@end

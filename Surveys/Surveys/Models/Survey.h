//
//  Survey.h
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Company, Question;

@interface Survey : NSManagedObject

@property (nonatomic, retain) NSString * info;
@property (nonatomic, retain) NSString * languageCode;
@property (nonatomic, retain) NSNumber * surveyId;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Company *company;
@property (nonatomic, retain) NSSet *questions;
@end

@interface Survey (CoreDataGeneratedAccessors)

- (void)addQuestionsObject:(Question *)value;
- (void)removeQuestionsObject:(Question *)value;
- (void)addQuestions:(NSSet *)values;
- (void)removeQuestions:(NSSet *)values;

@end

//
//  Question.h
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AnswerFormat1, AnswerFormat2, AnswerFormat3, Comments, History, Survey;

@interface Question : NSManagedObject

@property (nonatomic, retain) NSNumber * format;
@property (nonatomic, retain) NSString * languageCode;
@property (nonatomic, retain) NSString * question;
@property (nonatomic, retain) NSNumber * questionId;
@property (nonatomic, retain) NSNumber * sequence;
@property (nonatomic, retain) AnswerFormat1 *answerFormat1;
@property (nonatomic, retain) AnswerFormat2 *answerFormat2;
@property (nonatomic, retain) AnswerFormat3 *answerFormat3;
@property (nonatomic, retain) NSSet *comments;
@property (nonatomic, retain) NSSet *history;
@property (nonatomic, retain) Survey *survey;
@end

@interface Question (CoreDataGeneratedAccessors)

- (void)addCommentsObject:(Comments *)value;
- (void)removeCommentsObject:(Comments *)value;
- (void)addComments:(NSSet *)values;
- (void)removeComments:(NSSet *)values;

- (void)addHistoryObject:(History *)value;
- (void)removeHistoryObject:(History *)value;
- (void)addHistory:(NSSet *)values;
- (void)removeHistory:(NSSet *)values;

@end

//
//  Table.m
//  Surveys
//
//  Created by Raúl Blánquez on 27/3/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "Table.h"
#import "Comments.h"
#import "Company.h"
#import "History.h"


@implementation Table

@dynamic tableId;
@dynamic comments;
@dynamic company;
@dynamic history;

@end

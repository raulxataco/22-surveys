//
//  DeviceUtils.h
//  DigitalHealth
//
//  Created by Daniel Ruiz on 21/05/14.
//  Copyright (c) 2014 Axa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DeviceUtils : NSObject

+ (NSString *)deviceLanguage;
+ (NSString *)deviceOSVersion;
+ (NSString *)deviceID;
+ (NSString *)appVersion;
+ (NSString *)appDevelopmentRegion;
+ (NSString *)appName;
+ (NSString *)appURLSchema;

@end

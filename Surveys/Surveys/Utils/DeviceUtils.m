//
//  DeviceUtils.m
//  DigitalHealth
//
//  Created by Daniel Ruiz on 21/05/14.
//  Copyright (c) 2014 Axa. All rights reserved.
//

#import "DeviceUtils.h"
#import "GlobalHeader.h"

@implementation DeviceUtils

+ (NSString *)deviceLanguage
{
    NSString *ret = [[NSLocale preferredLanguages] objectAtIndex:0];
    DLog(@"Device language: %@", ret);
    return ret;
}

+ (NSString *)deviceOSVersion
{
    NSString *ret = [[UIDevice currentDevice] systemVersion];
    DLog(@"OS Version: %@", ret);
    return ret;
}

+ (NSString *)deviceID
{
    
    NSUUID *deviceID = [[UIDevice currentDevice] identifierForVendor];
    NSString *ret = deviceID.UUIDString;
    DLog(@"Device ID: %@", ret);
    return ret;
}

+ (NSString *)appVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

+ (NSString *)appDevelopmentRegion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDevelopmentRegion"];
}

+ (NSString *)appName
{
    NSBundle *bundle = [NSBundle mainBundle];
    NSDictionary *info = [bundle infoDictionary];
    
    return [info objectForKey:@"CFBundleDisplayName"];
}

+ (NSString *)appURLSchema
{
    NSBundle *bundle = [NSBundle mainBundle];
    NSDictionary *info = [bundle infoDictionary];
    
    NSArray *urlTypes = info[@"CFBundleURLTypes"];
    if (urlTypes.count > 0)
    {
        NSDictionary *urlType = urlTypes.firstObject;
        NSArray *urlSchemes = urlType[@"CFBundleURLSchemes"];
        
        return urlSchemes.firstObject;
    }
    return Nil;
}

@end

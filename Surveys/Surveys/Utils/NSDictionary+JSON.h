//
//  NSDictionary+JSON.h
//  DigitalHealth
//
//  Created by Daniel Ruiz on 21/05/14.
//  Copyright (c) 2014 Axa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSON)

- (NSString *)jsonStringWithPrettyPrint:(BOOL) prettyPrint;

@end

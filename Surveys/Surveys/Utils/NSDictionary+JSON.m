//
//  NSDictionary+JSON.m
//  DigitalHealth
//
//  Created by Daniel Ruiz on 21/05/14.
//  Copyright (c) 2014 Axa. All rights reserved.
//

#import "NSDictionary+JSON.h"
#import "GlobalHeader.h"

@implementation NSDictionary (JSON)

- (NSString *)jsonStringWithPrettyPrint:(BOOL)prettyPrint
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:(NSJSONWritingOptions)(prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (!jsonData)
    {
        DLog(@"Error: %@", error.localizedDescription);
        return @"{}";
    }
    else
    {
        return [[NSString alloc] initWithData:jsonData
                                     encoding:NSUTF8StringEncoding];
    }
}

@end

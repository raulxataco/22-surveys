//
//  Utils.m
//  Surveys
//
//  Created by Raúl Blánquez on 29/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "Utils.h"
#import <UIKit/UIKit.h>

@implementation Utils

+ (BOOL)doIHaveCam
{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

@end

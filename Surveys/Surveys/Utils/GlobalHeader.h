//
//  GlobalHeader.h
//  Surveys
//
//  Created by Raúl Blánquez on 29/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#ifndef Surveys_GlobalHeader_h
#define Surveys_GlobalHeader_h

//Usar Delog para logs que solo tienen que estar en Debug

#define XCODE_COLORS_ESCAPE     @"\033["
#define XCODE_COLORS_RESET_FG   XCODE_COLORS_ESCAPE @"fg;" //Clear any foreground color
#define XCODE_COLORS_RESET_BG   XCODE_COLORS_ESCAPE @"bg;" //Clear any background color
#define XCODE_COLORS_RESET      XCODE_COLORS_ESCAPE @";"   //Clear any foreground or background color
#define XCODE_COLORS_FORE_BLUE  XCODE_COLORS_ESCAPE @"fg102,255,255;"
#define XCODE_COLORS_FORE_WHITE XCODE_COLORS_ESCAPE @"fg255,255,255;"
#define XCODE_COLORS_FORE_RED   XCODE_COLORS_ESCAPE @"fg220,0,0;"
#define XCODE_COLORS_BACK_RED   XCODE_COLORS_ESCAPE @"bg220,0,0;"
#define XCODE_COLORS_BACK_WHITE XCODE_COLORS_ESCAPE @"bg255,255,255;"

#ifdef DEBUG
#define DLog(fmt, ...)      NSLog(                    (@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__,                         ##__VA_ARGS__);
#define DLogError(fmt, ...) NSLog((XCODE_COLORS_FORE_RED @"%s" fmt XCODE_COLORS_RESET), __PRETTY_FUNCTION__, ##__VA_ARGS__);
//#define CLogInfo(fmt, ...)  NSLog((XCODE_COLORS_FORE_BLUE XCODE_COLORS_BACK_WHITE @"%s" fmt XCODE_COLORS_RESET), __PRETTY_FUNCTION__, ##__VA_ARGS__);
#define DLogInfo(fmt, ...)  NSLog((XCODE_COLORS_FORE_BLUE @"%s" fmt XCODE_COLORS_RESET), __PRETTY_FUNCTION__, ##__VA_ARGS__);

//#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define DLog(...) {}
#endif

//Usar RLog para logs que tienen que estar en Release
#define RLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

//CoreData entities
#define kCompanyEntity  @"Company"
#define kServerEntity   @"Server"
#define kTableEntity    @"Table"
#define kSurveyEntity   @"Survey"
#define kQuestionEntity @"Question"
#define kAnswer1Entity  @"AnswerFormat1"
#define kAnswer2Entity  @"AnswerFormat2"
#define kAnswer3Entity  @"AnswerFormat3"
#define kLanguageEntity @"Language"
#define KHistoryEntity  @"History"
#define kCommentEntity  @"Comments"
#define kMessageEntity  @"Message"

//Photo picker
#define kNewPhotoButtonIndex        0
#define kExistingPhotoButtonIndex   1

//Notifications
#define kRotationGestureNotification @"rotationGesture"

//Question Formats
typedef NS_ENUM(NSInteger, ServerQuestionType) {
    kQuestion5Answers = 1,
    kQuestion3Answers,
    kQuestion2Answers,
    kQuestionComments,
    kQuestionStars
};

//UserDefault Keys
#define kUserDefRegisteredUser @"registeredUser"
#define kUserDefTutorialDone @"tutorialDone"

#endif

//
//  ImageCropViewController.h
//  DigitalHealth
//
//  Created by Daniel Ruiz on 07/05/14.
//  Copyright (c) 2014 Axa. All rights reserved.
//

#import "ImageCropViewController.h"

#import "ImageCropView.h"

#define kCropToolbarHeight 44
#define kCroppedPhotoSize 300

@interface ImageCropViewController ()

@property (strong, nonatomic) ImageCropView *imageCropView;

@end

@implementation ImageCropViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect initialViewFrame = CGRectMake(0.0f,
                                         0.0f,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height - kCropToolbarHeight);
    
    self.imageCropView = [[ImageCropView alloc] initWithFrame:initialViewFrame];
    [self.view addSubview:self.imageCropView];
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f,
                                                                     self.imageCropView.frame.size.height,
                                                                     self.view.frame.size.width,
                                                                     kCropToolbarHeight)];
    [self.view addSubview:toolbar];
    
    UIBarButtonItem *cancelItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(cancelCropping)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                           target:nil
                                                                           action:nil];
    
    UIBarButtonItem *cropItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"OK", nil)
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(cropImage)];
    
    [toolbar setItems:[NSArray arrayWithObjects:cancelItem, space, cropItem, nil]];
    
    self.imageCropView.image = self.sourceImage;
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CGRect initialViewFrame = CGRectMake(0.0f,
                                         0.0f,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height - kCropToolbarHeight);

    self.imageCropView.frame = initialViewFrame;
}

#pragma mark - UIBarButtonItems Actions

- (void)cancelCropping
{
    [self.delegate imageCropViewControllerDidCancel:self];
}

- (void)cropImage
{
    UIImage* imageInBounds = [self.imageCropView getCroppedImage];
    
    [self.delegate imageCropViewController:self
                              didCropImage:imageInBounds];
}

@end

//
//  ImageCropViewController.h
//  DigitalHealth
//
//  Created by Daniel Ruiz on 07/05/14.
//  Copyright (c) 2014 Axa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImageCropViewController;

@protocol ImageCropViewControllerDelegate <NSObject>

- (void)imageCropViewController:(ImageCropViewController *)imageCropViewController
                   didCropImage:(UIImage *)croppedImage;

- (void)imageCropViewControllerDidCancel:(ImageCropViewController *)imageCropViewController;

@end

@interface ImageCropViewController : UIViewController

@property (weak, nonatomic) id<ImageCropViewControllerDelegate> delegate;
@property (strong, nonatomic) UIImage* sourceImage;

@end

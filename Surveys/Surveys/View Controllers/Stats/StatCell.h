//
//  StatCell.h
//  Surveys
//
//  Created by mobdev1 on 20/11/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *statValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *statAnswerLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthValueConstraint;
@property (weak, nonatomic) IBOutlet UILabel *commentDateLabel;
@end

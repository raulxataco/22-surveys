//
//  StatsVC.m
//  Surveys
//
//  Created by mobdev1 on 19/11/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "StatsVC.h"
#import "GlobalHeader.h"
#import "DataManager.h"
#import "SurveyDataAccess.h"
#import "HistoryDataAccess.h"
#import "CompanyDataAccess.h"
#import "History.h"
#import "Comments.h"
#import "Question.h"
#import "AnswerFormat1.h"
#import "AnswerFormat2.h"
#import "AnswerFormat3.h"
#import "StatCell.h"
#import "ChangeSurveyVC.h"
#import "SCLAlertView.h"
#import "UploadManager.h"

@interface StatsVC () <UITableViewDataSource, UITableViewDelegate, ChangeSurveyDelegate, UploadSurveyDelegate>
{
    NSArray *_questionList;
    NSMutableArray *_answersList;
    Company *_company;
}
@property (weak, nonatomic) IBOutlet UILabel *startLabel;
@property (weak, nonatomic) IBOutlet UILabel *endLabel;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
@property (weak, nonatomic) IBOutlet UITableView *statsTableView;
@property (weak, nonatomic) IBOutlet UIButton *selectSurveyButton;
@property (weak, nonatomic) IBOutlet UIButton *uploadSurveyButton;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteSurveyButton;

@end

@implementation StatsVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];

    _company = [[CompanyDataAccess sharedManager] getCompany];

    [self _makeView];
    [self _createStats];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqual:@"surveySelection"])
    {
        ChangeSurveyVC *vc = segue.destinationViewController;
        vc.delegate2 = self;
    }
}

// *****************************************************************************
#pragma mark -                                          ChangeSurvey delegate
// *****************************************************************************
- (void)userChangedSurvey:(Survey *)survey
{
    _company.currentSurveyId = survey.surveyId;
    
    [DataManager saveContext];

    [self _makeView];
    [self _createStats];
    [self.statsTableView reloadData];
    
    [self.navigationController popViewControllerAnimated:YES];
}

// *****************************************************************************
#pragma mark -                                          UploadSurvey delegate
// *****************************************************************************
- (void)didFinishedUploadingSurvey
{
    [[HistoryDataAccess sharedManager] removeHistoryForSurveyId:_company.currentSurveyId];

    [self _createStats];
    [self.statsTableView reloadData];
}

// *****************************************************************************
#pragma mark -                                          Table view data source
// *****************************************************************************
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *item = _answersList[indexPath.section];
    
    ServerQuestionType questionType = [item[@"type"] integerValue];
    
    switch (questionType) {
        case kQuestion5Answers:
        case kQuestion3Answers:
        case kQuestion2Answers:
            return 25;
            break;
        case kQuestionComments:
            return 39;
            break;
        case kQuestionStars:
            return 25;
            break;
            
        default:
            break;
    }

    return 25;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    Question *question = _questionList[section];
    
    return question.question;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _questionList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if ([_answersList count] > 0 && [_answersList count] < section)
//    {
//        DLog(@"%@", _answersList[section]);
//        DLog(@"%@", _answersList[section][@"options"]);
        return [_answersList[section][@"options"] count];
//    }
//    else
//    {
//        return 0;
//    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    StatCell *cell;
    
    NSDictionary *item = _answersList[indexPath.section];
    
    ServerQuestionType questionType = [item[@"type"] integerValue];
    NSDictionary *option = item[@"options"][indexPath.row];

    switch (questionType) {
        case kQuestion5Answers:
        case kQuestion3Answers:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"statCell" forIndexPath:indexPath];
            cell.statAnswerLabel.text = [NSString stringWithFormat:@"%@", option[@"question"]];
            cell.widthValueConstraint.constant = [option[@"value"] floatValue] * 100;
            cell.statValueLabel.text  = [NSString stringWithFormat:@"%.0f%%", [option[@"value"] floatValue] * 100];
            break;
        }
        case kQuestion2Answers:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"statCell" forIndexPath:indexPath];
            
            NSString *field = [NSString stringWithFormat:@"%@", option[@"question"]];
            cell.statAnswerLabel.text = NSLocalizedString(field, Nil);
            cell.widthValueConstraint.constant = [option[@"value"] floatValue] * 100;
            cell.statValueLabel.text  = [NSString stringWithFormat:@"%.0f%%", [option[@"value"] floatValue] * 100];

            break;
        }
        case kQuestionComments:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"commentCell" forIndexPath:indexPath];
            cell.statAnswerLabel.text = option[@"comment"];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd/MM/yy"];
            cell.commentDateLabel.text = [formatter stringFromDate:option[@"date"]];

            break;
        }
        case kQuestionStars:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"pointsCell" forIndexPath:indexPath];
            cell.statAnswerLabel.text = @"";
            cell.statValueLabel.text  = [NSString stringWithFormat:@"%@ / 5", option[@"value"]];

            break;
        }
        default:
            break;
    }

    return cell;
}

// *****************************************************************************
#pragma mark -                                          Target actions
// *****************************************************************************
- (IBAction)openWebStats:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.xataco.com/surveys/xtc_surveys.php"]];
}

- (IBAction)deleteSurveys:(id)sender
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:NSLocalizedString(@"yes", Nil)
         actionBlock:^(void) {
             DLog(@"Deleting surveys....");
             
             [DataManager removeFromEntity:KHistoryEntity withPredicate:Nil];
             [DataManager removeFromEntity:kCommentEntity withPredicate:Nil];
             [DataManager saveContext];
             [self _makeView];
             [self _createStats];
             [self.statsTableView reloadData];

         }];
    
    [alert showWarning:self
                 title:NSLocalizedString(@"stats_delete_label", nil)
              subTitle:NSLocalizedString(@"stats_delete_description", Nil)
      closeButtonTitle:NSLocalizedString(@"no", Nil)
              duration:0.0f];
}

- (IBAction)doSelectSurvey:(id)sender
{
    DLog();
    [self performSegueWithIdentifier:@"surveySelection" sender:self];
}

- (IBAction)uploadSurveys:(id)sender
{
    [[UploadManager sharedManager] setDelegate:self];
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:NSLocalizedString(@"yes", Nil)
         actionBlock:^(void) {
             [[UploadManager sharedManager] uploadHistoryForCurrentSurvey];
         }];
    
    [alert showWarning:self
                 title:NSLocalizedString(@"main_survey_export_title", nil)
              subTitle:NSLocalizedString(@"main_survey_export_message", Nil)
      closeButtonTitle:NSLocalizedString(@"no", Nil)
              duration:0.0f];
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)_makeView
{
    Survey *survey = [[SurveyDataAccess sharedManager] getSurveyWithLanguageCode:[[NSLocale preferredLanguages] firstObject]];
    
    self.title           = survey.title;
    self.startLabel.text = NSLocalizedString(@"start", Nil);
    self.endLabel.text   = NSLocalizedString(@"end", Nil);
    
    [self.selectSurveyButton setTitle:NSLocalizedString(@"prepare_survey_change_button", nil) forState:UIControlStateNormal];
    [self.uploadSurveyButton setTitle:NSLocalizedString(@"stats_upload_button", Nil) forState:UIControlStateNormal];
    [self.deleteSurveyButton setTitle:NSLocalizedString(@"stats_delete_button", nil) forState:UIControlStateNormal];
    
    NSDictionary *range = [[HistoryDataAccess sharedManager] getHistoryRange];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yy HH:mm"];

    self.startDateLabel.text = range ? [formatter stringFromDate:range[@"start"]] : @"";
    self.endDateLabel.text   = range ? [formatter stringFromDate:range[@"end"]] : @"";
    
    BOOL userRegistered = [[NSUserDefaults standardUserDefaults] boolForKey:kUserDefRegisteredUser];
    self.uploadSurveyButton.hidden = !userRegistered;
}

- (void)_createStats
{
    //Get questions for survey
    _questionList = [[SurveyDataAccess sharedManager] getQuestionsForDeviceLanguage];
    
    _answersList = [[NSMutableArray alloc] init];
    
    NSInteger totalRespuestas = 0;
    
    for (Question *question in _questionList)
    {
        DLog(@"%@", question.question);
        
        NSInteger totalAnswers = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                      andSurveyId:_company.currentSurveyId];
        
        if (totalRespuestas == 0) { totalRespuestas = totalAnswers; };
        
        DLog(@"Total respuestas = %d", (int)totalAnswers);
        if (totalAnswers == 0)
        {
            totalAnswers++;
        }
        
        ServerQuestionType questionType = [question.format integerValue];
        
        switch (questionType) {
            case kQuestion5Answers:
            {
                if (!question.answerFormat1) break;
                
                NSInteger totalOption1 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@1
                                                                                              andSurveyId:_company.currentSurveyId];
                NSInteger totalOption2 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@2
                                                                                              andSurveyId:_company.currentSurveyId];
                NSInteger totalOption3 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@3
                                                                                              andSurveyId:_company.currentSurveyId];
                NSInteger totalOption4 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@4
                                                                                              andSurveyId:_company.currentSurveyId];
                NSInteger totalOption5 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@5
                                                                                              andSurveyId:_company.currentSurveyId];

                
                DLog(@"%@ = %d", question.answerFormat1.option1, (int)totalOption1);
                DLog(@"%@ = %d", question.answerFormat1.option2, (int)totalOption2);
                DLog(@"%@ = %d", question.answerFormat1.option3, (int)totalOption3);
                DLog(@"%@ = %d", question.answerFormat1.option4, (int)totalOption4);
                DLog(@"%@ = %d", question.answerFormat1.option5, (int)totalOption5);
                
                NSDictionary *option1 = @{ @"value"    : [NSNumber numberWithFloat:(float)totalOption1/totalAnswers],
                                           @"question" : question.answerFormat1.option1 };
                NSDictionary *option2 = @{ @"value"    : [NSNumber numberWithFloat:(float)totalOption2/totalAnswers],
                                           @"question" : question.answerFormat1.option2 };
                NSDictionary *option3 = @{ @"value"    : [NSNumber numberWithFloat:(float)totalOption3/totalAnswers],
                                           @"question" : question.answerFormat1.option3 };
                NSDictionary *option4 = @{ @"value"    : [NSNumber numberWithFloat:(float)totalOption4/totalAnswers],
                                           @"question" : question.answerFormat1.option4 };
                NSDictionary *option5 = @{ @"value"    : [NSNumber numberWithFloat:(float)totalOption5/totalAnswers],
                                           @"question" : question.answerFormat1.option5 };
                
                NSDictionary *answers = @{@"type" : question.format, @"options" : @[option1, option2, option3, option4, option5]};
                
                [_answersList addObject:answers];
                
                break;
            }
            case kQuestion3Answers:
            {
                if (!question.answerFormat2) break;

                NSInteger totalOption1 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@1
                                                                                              andSurveyId:_company.currentSurveyId];
                NSInteger totalOption2 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@2
                                                                                              andSurveyId:_company.currentSurveyId];
                NSInteger totalOption3 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@3
                                                                                              andSurveyId:_company.currentSurveyId];
                
                DLog(@"%@ = %d", question.answerFormat2.option1, (int)totalOption1);
                DLog(@"%@ = %d", question.answerFormat2.option2, (int)totalOption2);
                DLog(@"%@ = %d", question.answerFormat2.option3, (int)totalOption3);
                
                NSDictionary *option1 = @{ @"value"    : [NSNumber numberWithFloat:(float)totalOption1/totalAnswers],
                                           @"question" : question.answerFormat2.option1 };
                NSDictionary *option2 = @{ @"value"    : [NSNumber numberWithFloat:(float)totalOption2/totalAnswers],
                                           @"question" : question.answerFormat2.option2 };
                NSDictionary *option3 = @{ @"value"    : [NSNumber numberWithFloat:(float)totalOption3/totalAnswers],
                                           @"question" : question.answerFormat2.option3 };
                
                NSDictionary *answers = @{@"type" : question.format, @"options" : @[option1, option2, option3]};

                [_answersList addObject:answers];
                
                break;
            }
            case kQuestion2Answers:
            {
                if (!question.answerFormat3) break;

                NSInteger totalOption1 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@1
                                                                                              andSurveyId:_company.currentSurveyId];
                NSInteger totalOption2 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@2
                                                                                              andSurveyId:_company.currentSurveyId];
                
                DLog(@"%@ = %d", question.answerFormat3.option1, (int)totalOption1);
                DLog(@"%@ = %d", question.answerFormat3.option2, (int)totalOption2);
                
                NSDictionary *option1 = @{ @"value"    : [NSNumber numberWithFloat:(float)totalOption1/totalAnswers],
                                           @"question" : question.answerFormat3.option1 };
                NSDictionary *option2 = @{ @"value"    : [NSNumber numberWithFloat:(float)totalOption2/totalAnswers],
                                           @"question" : question.answerFormat3.option2 };
                
                NSDictionary *answers = @{@"type" : question.format, @"options" : @[option1, option2]};
                
                [_answersList addObject:answers];

                break;
            }
            case kQuestionStars:
            {
                NSInteger totalOption1 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@1
                                                                                              andSurveyId:_company.currentSurveyId];
                NSInteger totalOption2 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@2
                                                                                              andSurveyId:_company.currentSurveyId];
                NSInteger totalOption3 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@3
                                                                                              andSurveyId:_company.currentSurveyId];
                NSInteger totalOption4 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@4
                                                                                              andSurveyId:_company.currentSurveyId];
                NSInteger totalOption5 = [[HistoryDataAccess sharedManager] getNumberOfAnswersForQuestion:question.questionId
                                                                                                andOption:@5
                                                                                              andSurveyId:_company.currentSurveyId];
                
                DLog(@"1 Estrella  = %d", (int)totalOption1);
                DLog(@"2 Estrella  = %d", (int)totalOption2);
                DLog(@"3 Estrella  = %d", (int)totalOption3);
                DLog(@"4 Estrella  = %d", (int)totalOption4);
                DLog(@"5 Estrella  = %d", (int)totalOption5);
                
                NSDictionary *option1 = @{ @"value"    : [NSNumber numberWithFloat:(float)(totalOption1+totalOption2*2+totalOption3*3+totalOption4*4+totalOption5*5)/totalAnswers],
                                           @"question" : @"stars" };
                
                NSDictionary *answers = @{@"type" : question.format, @"options" : @[option1]};

                [_answersList addObject:answers];
                
                break;
            }
            case kQuestionComments:
            {
                NSMutableArray *comments = [[NSMutableArray alloc] init];
                
                for (Comments *comment in _company.comments)
                {
                    if ([comment.question.survey.surveyId isEqualToNumber:_company.currentSurveyId])
                    {
                        NSDictionary *commentDict = @{@"date" : comment.date, @"comment" : comment.answer};
                        [comments addObject:commentDict];
                    }
                }
                
                if ([comments count] == 0)
                {
                    NSDictionary *commentDict = @{@"date" : [NSDate date], @"comment" : NSLocalizedString(@"stats_nocomment_label", nil)};
                    [comments addObject:commentDict];
                }
                
                NSDictionary *answers = @{@"type" : question.format, @"options" : comments};
                
                [_answersList addObject:answers];
                
                break;
            }
            default:
                break;
        }
    }
    
    self.totalLabel.text = [NSString stringWithFormat:@"%@ %d", NSLocalizedString(@"stats_total_surveys", nil), (int)totalRespuestas];
}

@end

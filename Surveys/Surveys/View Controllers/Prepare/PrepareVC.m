//
//  PrepareVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "PrepareVC.h"
#import "ServersTVC.h"
#import "ServerCell.h"
#import "GlobalHeader.h"
#import "Utils.h"
#import "ImageCropViewController.h"
#import "ServerDataAccess.h"
#import "TableDataAccess.h"
#import "TablesTVC.h"
#import "TableCell.h"
#import "CustomersTVC.h"
#import "DataManager.h"
#import "CompanyDataAccess.h"
#import "SVProgressHUD.h"
#import "SurveyDataAccess.h"
#import "LanguageDataAccess.h"
#import "ChangeSurveyVC.h"

@interface PrepareVC () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ServersTableDelegate, ImageCropViewControllerDelegate, ChangeSurveyDelegate>
{
    ServerCell  *_currentServerCell;
    BOOL        _editingLogo;
    Company     *_company;
    Survey      *_currentSurvey;
}
@property (weak, nonatomic) IBOutlet UILabel *surveyPreparationLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentSurveyLabel;
@property (weak, nonatomic) IBOutlet UITableView *serversTable;
@property (weak, nonatomic) IBOutlet UIButton *changeSurveyButton;
@property (weak, nonatomic) IBOutlet UITableView *tablesTable;
@property (weak, nonatomic) IBOutlet UITableView *customersTable;
@property (strong, nonatomic) ServersTVC *serversTVC;
@property (strong, nonatomic) TablesTVC *tablesTVC;
@property (strong, nonatomic) CustomersTVC *customersTVC;
@property (weak, nonatomic) IBOutlet UILabel *serversLabel;
@property (weak, nonatomic) IBOutlet UILabel *tablesLabel;
@property (weak, nonatomic) IBOutlet UILabel *customersLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@end

@implementation PrepareVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _makeView];
    [self _localizeLabels];
    [self _initData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqual:@"surveySelection"])
    {
        ChangeSurveyVC *vc = segue.destinationViewController;
        vc.delegate2 = self;
    }
}

// *****************************************************************************
#pragma mark -                                          Custom getters & setters
// *****************************************************************************
- (ServersTVC *)serversTVC
{
    if (!_serversTVC)
    {
        _serversTVC = [[ServersTVC alloc] init];
    }
    
    return _serversTVC;
}

- (TablesTVC *)tablesTVC
{
    if (!_tablesTVC)
    {
        _tablesTVC = [[TablesTVC alloc] init];
    }
    
    return _tablesTVC;
}

- (CustomersTVC *)customersTVC
{
    if (!_customersTVC)
    {
        _customersTVC = [[CustomersTVC alloc] init];
    }
    
    return _customersTVC;
}

// *****************************************************************************
#pragma mark -                                          UIActionSheet Delegate
// *****************************************************************************
#warning Se está llamando 2 veces. La segunda con buttonIndex = 2
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case kNewPhotoButtonIndex:
            [self _pickImageWithSourceType:UIImagePickerControllerSourceTypeCamera];
            break;
            
        case kExistingPhotoButtonIndex:
            [self _pickImageWithSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
            break;
            
        default:
            //_editingLogo = NO;
            break;
    }
}

// *****************************************************************************
#pragma mark -                                          ChangeSurvey delegate
// *****************************************************************************
- (void)userChangedSurvey:(Survey *)survey
{
    _company.currentSurveyId = survey.surveyId;
    
    [DataManager saveContext];
    
    self.currentSurveyLabel.text = survey.title;
    
    [self.navigationController popViewControllerAnimated:YES];
}

// *****************************************************************************
#pragma mark -                                          ServersTableVC delegate
// *****************************************************************************
- (void)pickPhoto:cell
{
    DLog();
    
    _currentServerCell = cell;
    
    if ([Utils doIHaveCam])
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:NSLocalizedString(@"imagepicker_photo_new", nil), NSLocalizedString(@"imagepicker_photo_existing", nil), nil];
        
        //[actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
        [actionSheet showInView:self.view];
    }
    else
    {
        [self _pickImageWithSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    }
}

// *****************************************************************************
#pragma mark -                                  UIImagePickerController Delegate
// *****************************************************************************
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES
                               completion:^{
                                   UIImage *pickedImage = (UIImage *)[info objectForKey:@"UIImagePickerControllerOriginalImage"];
                                   if (pickedImage)
                                   {
                                       if (!_editingLogo)
                                       {
                                           ImageCropViewController *imageCropViewController = [[ImageCropViewController alloc] init];
                                           imageCropViewController.delegate = self;
                                           imageCropViewController.sourceImage = pickedImage;
                                           
                                           [self presentViewController:imageCropViewController
                                                              animated:YES
                                                            completion:nil];
                                       }
                                       else
                                       {
                                           _editingLogo = NO;
                                           
                                           _company = [[CompanyDataAccess sharedManager] getCompany];
                                           NSData *imageData = UIImagePNGRepresentation(pickedImage);
                                           _company.logo = imageData;
                                           
                                           [DataManager saveContext];
                                       }
                                   }
                               }];
}

// *****************************************************************************
#pragma mark -                                  ImageCropViewController Delegate
// *****************************************************************************
- (void)imageCropViewController:(ImageCropViewController *)imageCropViewController didCropImage:(UIImage *)croppedImage
{
    _currentServerCell.serverImage.image = croppedImage;
    
    Server *server = [ServerDataAccess getServerWithId:_currentServerCell.serverId];
    NSData *imageData = UIImagePNGRepresentation(croppedImage);
    server.photo = imageData;
    
    [DataManager saveContext];
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

- (void)imageCropViewControllerDidCancel:(ImageCropViewController *)imageCropViewController
{
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

// *****************************************************************************
#pragma mark -                                          Target Actions
// *****************************************************************************
- (IBAction)loadLogo:(id)sender
{
    DLog();
    
    _editingLogo = YES;

    if ([Utils doIHaveCam])
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:NSLocalizedString(@"imagepicker_photo_new", nil), NSLocalizedString(@"imagepicker_photo_existing", nil), nil];
        
        [actionSheet showInView:self.view];
    }
    else
    {
        [self _pickImageWithSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    }
}

- (void)startSurvey
{
    //if (self.serversTVC.selected && self.tablesTVC.selected && self.customersTVC.selected)
    if (self.serversTVC.selected)
    {
        [self performSegueWithIdentifier:@"survey" sender:self];
    }
    else
    {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"prepare_survey_select_label", Nil)];
    }
}

- (IBAction)unwindToPrepare:(UIStoryboardSegue *)unwindSegue
{
    DLog();
}

- (IBAction)doChangeSurvey:(id)sender
{
    DLog();
    [self performSegueWithIdentifier:@"surveySelection" sender:self];
}

// *****************************************************************************
#pragma mark -                                          Private Methods
// *****************************************************************************
- (void)_makeView
{
    self.title = NSLocalizedString(@"prepare_survey_title", Nil);
    self.surveyPreparationLabel.text = NSLocalizedString(@"prepare_survey_label", Nil);
    
    UIBarButtonItem *startItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"prepare_survey_start_button", Nil)
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(startSurvey)];
    self.navigationItem.rightBarButtonItems = @[startItem];
    
    _company = [[CompanyDataAccess sharedManager] getCompany];
    
    self.title= _company.name;
    
    if (_company.backgroundImage)
    {
        self.backgroundImage.image = [UIImage imageWithData:_company.backgroundImage];
    }
}

- (void)_localizeLabels
{
    self.serversLabel.text   = NSLocalizedString(@"prepare_survey_servers_label", nil);
    self.tablesLabel.text    = NSLocalizedString(@"prepare_survey_tables_label", nil);
    self.customersLabel.text = NSLocalizedString(@"prepare_survey_customers_label", nil);
    
    [self.changeSurveyButton setTitle:NSLocalizedString(@"prepare_survey_change_button", nil) forState:UIControlStateNormal];
}

- (void)_initData
{
    NSString *locale = [[NSLocale preferredLanguages] firstObject];
    
    DLog(@"locale = %@", locale);
    
    _currentSurvey = [[SurveyDataAccess sharedManager] getSurveyWithLanguageCode:locale];
    
    self.currentSurveyLabel.text = _currentSurvey.title;

    [self.serversTable setDataSource:self.serversTVC];
    [self.serversTable setDelegate:self.serversTVC];
    self.serversTVC.delegate             = self;
    self.serversTVC.serverList           = [[ServerDataAccess sharedManager] getServersForCompany:_company.idCompany];
    self.serversTVC.view                 = self.serversTVC.tableView;
    self.serversTVC.view.backgroundColor = [UIColor clearColor];
    
    [self.tablesTable setDataSource:self.tablesTVC];
    [self.tablesTable setDelegate:self.tablesTVC];
    self.tablesTVC.tableList = [TableDataAccess getTables];
    self.tablesTVC.view      = self.tablesTVC.tableView;

    [self.customersTable setDataSource:self.customersTVC];
    [self.customersTable setDelegate:self.customersTVC];
    self.customersTVC.view = self.customersTVC.tableView;
        
    _editingLogo = NO;
}

- (void)_pickImageWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType = sourceType;
    
    [self presentViewController:imagePickerController
                       animated:YES
                     completion:nil];
}

@end

//
//  CustomersTVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 01/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "CustomersTVC.h"
#import "TableCell.h"
#import "CompanyDataAccess.h"

@interface CustomersTVC ()
{
    NSMutableArray *_itemList;
}

@end

@implementation CustomersTVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _initData];
}

// *****************************************************************************
#pragma mark -                                          Table view data source
// *****************************************************************************
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _itemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customersCell" forIndexPath:indexPath];
    
    cell.tableIdLabel.text = _itemList[indexPath.row];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

// *****************************************************************************
#pragma mark -                                          Table view delegate
// *****************************************************************************
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selected = YES;
}

// *****************************************************************************
#pragma mark -                                          Private Methods
// *****************************************************************************
- (void)_initData
{
    Company *company = [[CompanyDataAccess sharedManager] getCompany];
    
    int customersNumber = [company.customers intValue];
    
    _itemList = [[NSMutableArray alloc] initWithCapacity:customersNumber+1];

    for (int i = 1; i <= customersNumber; i++)
    {
        [_itemList addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    [_itemList addObject:[NSString stringWithFormat:@"+%d",customersNumber]];
}

@end

//
//  CustomersTVC.h
//  Surveys
//
//  Created by Raúl Blánquez on 01/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomersTVC : UITableViewController

@property (nonatomic, assign) BOOL selected;

@end

//
//  ChangeSurveyVC.m
//  Surveys
//
//  Created by mobdev1 on 10/11/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "ChangeSurveyVC.h"
#import "SurveyDataAccess.h"
#import "Survey.h"

@interface ChangeSurveyVC ()
{
    NSArray *_surveysList;
}
@property (weak, nonatomic) IBOutlet UILabel *surveySelectLabel;
@property (weak, nonatomic) IBOutlet UITableView *surveysTable;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@end

@implementation ChangeSurveyVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self _localizeLabels];
    [self _loadSurveys];
}

// *****************************************************************************
#pragma mark -                                          Table view data source
// *****************************************************************************
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _surveysList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"surveyCell" forIndexPath:indexPath];
    
    Survey *survey = _surveysList[indexPath.row];
    
    cell.textLabel.text = survey.title;
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

// *****************************************************************************
#pragma mark -                                          Table view delegate
// *****************************************************************************
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate2 userChangedSurvey:_surveysList[indexPath.row]];
}

// *****************************************************************************
#pragma mark -                                          Private Methods
// *****************************************************************************
- (void)_localizeLabels
{
    self.surveySelectLabel.text = NSLocalizedString(@"change_survey_label", Nil);
}

- (void)_loadSurveys
{
    NSString *locale = [[NSLocale preferredLanguages] firstObject];
    _surveysList = [[SurveyDataAccess sharedManager] getSurveysWithLanguageCode:locale];
}

@end

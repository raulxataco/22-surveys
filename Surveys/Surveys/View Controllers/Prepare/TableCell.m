//
//  TableCell.m
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "TableCell.h"

@implementation TableCell

- (void)awakeFromNib
{
    self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    self.selectedBackgroundView.backgroundColor = [UIColor darkGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  ServersTVC.h
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerCell.h"

@protocol ServersTableDelegate
- (void)pickPhoto:(ServerCell *)serverCell;
@end

@interface ServersTVC : UITableViewController

@property (nonatomic, assign) id<ServersTableDelegate> delegate;
@property (nonatomic, strong) NSArray *serverList;
@property (nonatomic, assign) BOOL selected;

@end

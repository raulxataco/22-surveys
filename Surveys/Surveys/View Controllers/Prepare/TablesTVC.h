//
//  TablesTVC.h
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TablesTVC : UITableViewController

@property (nonatomic, strong) NSArray *tableList;
@property (nonatomic, assign) BOOL selected;

@end

//
//  TablesTVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "TablesTVC.h"
#import "TableCell.h"
#import "Table.h"

@interface TablesTVC ()

@end

@implementation TablesTVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

// *****************************************************************************
#pragma mark -                                          Table view data source
// *****************************************************************************
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableCell" forIndexPath:indexPath];
    
    Table *table = self.tableList[indexPath.row];
    
    cell.tableIdLabel.text = table.tableId;
    cell.tableId = table.tableId;
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

// *****************************************************************************
#pragma mark -                                          Table view delegate
// *****************************************************************************
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selected = YES;
}

@end

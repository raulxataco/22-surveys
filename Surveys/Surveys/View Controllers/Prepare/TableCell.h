//
//  TableCell.h
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *tableIdLabel;
@property (nonatomic, strong) NSString *tableId;

@end

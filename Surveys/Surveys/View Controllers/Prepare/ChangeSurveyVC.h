//
//  ChangeSurveyVC.h
//  Surveys
//
//  Created by mobdev1 on 10/11/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "BaseVC.h"
#import "Survey.h"

@protocol ChangeSurveyDelegate
- (void)userChangedSurvey:(Survey *)survey;
@end

@interface ChangeSurveyVC : BaseVC

@property (nonatomic, weak) id<ChangeSurveyDelegate> delegate2;

@end

//
//  ServersTVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "ServersTVC.h"
#import "ServerCell.h"
#import "GlobalHeader.h"
#import "Utils.h"
#import "Server.h"
@import QuartzCore;

@interface ServersTVC () <ServerCellDelegate>

@end

@implementation ServersTVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    DLog();
}

// *****************************************************************************
#pragma mark -                                          Table view data source
// *****************************************************************************
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.serverList.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.serverList.count == 1)
    {
        cell.selected = YES;
        self.selected = YES;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ServerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"serverCell" forIndexPath:indexPath];
    
    Server *server = self.serverList[indexPath.row];
    
    cell.serverNameLabel.text = server.name;
    cell.serverImage.image = [UIImage imageWithData:server.photo];
    
    cell.serverImage.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.serverImage.layer.borderWidth = 2.0f;
    
    cell.serverId = server.serverId;
    cell.delegate = self;
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

// *****************************************************************************
#pragma mark -                                          Table view delegate
// *****************************************************************************
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selected = YES;
}

// *****************************************************************************
#pragma mark -                                          ServerCell delegate
// *****************************************************************************
- (void)pickPhoto:(ServerCell *)cell
{
    DLog();
    [self.delegate pickPhoto:cell];
}

@end

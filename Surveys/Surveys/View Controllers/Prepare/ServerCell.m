//
//  ServerCell.m
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "ServerCell.h"

@implementation ServerCell

- (void)awakeFromNib
{
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(onPhotoTap:)];
    
    [self.serverImage addGestureRecognizer:tapGestureRecognizer];
    
    self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    self.selectedBackgroundView.backgroundColor = [UIColor darkGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)onPhotoTap:(id)sender
{
    [self.delegate pickPhoto:self];
}

@end

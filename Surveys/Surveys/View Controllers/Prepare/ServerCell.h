//
//  ServerCell.h
//  Surveys
//
//  Created by Raúl Blánquez on 30/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircularImageView.h"

@class ServerCell;

@protocol ServerCellDelegate
- (void)pickPhoto:(ServerCell *)cell;
@end

@interface ServerCell : UITableViewCell

@property (nonatomic, assign) id<ServerCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet CircularImageView *serverImage;
@property (weak, nonatomic) IBOutlet UILabel *serverNameLabel;
@property (strong, nonatomic) NSNumber *serverId;
@end

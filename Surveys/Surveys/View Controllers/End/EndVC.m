//
//  EndVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 20/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "EndVC.h"
#import "GlobalHeader.h"
#import "SurveyDataAccess.h"
#import "QuestionVC.h"
#import "CompanyDataAccess.h"
#import "LanguageDataAccess.h"

@interface EndVC ()

@property (weak, nonatomic) IBOutlet UILabel *thanksLabel;
@property (weak, nonatomic) IBOutlet UIButton *surveyButton;
@end

@implementation EndVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isEndView = YES;
    
    [self _makeView];    
}

// *****************************************************************************
#pragma mark -                                          Target actions
// *****************************************************************************
- (IBAction)doNewSurvey:(id)sender
{
    
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)_makeView
{
    NSString *languageCode = [SurveyDataAccess sharedManager].languageCode;
    NSString *language = [NSString stringWithFormat:@"SurveyLang_%@", languageCode];
    //self.thanksLabel.text = NSLocalizedStringFromTable(@"survey_end_label", language, nil);
    NSString *message = [[CompanyDataAccess sharedManager] getThanksMessageForLanguage:languageCode];
    self.thanksLabel.text = [NSString stringWithFormat:@"%@\n%@",message, NSLocalizedStringFromTable(@"survey_end_label", language, nil)];
    
    [self.surveyButton setTitle:NSLocalizedStringFromTable(@"survey_new_label", language, Nil) forState:UIControlStateNormal];
}

@end

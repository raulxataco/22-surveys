//
//  BaseVC.h
//  Surveys
//
//  Created by Raúl Blánquez on 05/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController

@property (nonatomic, assign) BOOL cancelable;
@property (nonatomic, assign) BOOL isEndView;

- (void)userDidRotationGesture;

@end

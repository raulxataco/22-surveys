//
//  BaseVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 05/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "BaseVC.h"
#import "GlobalHeader.h"
#import "SurveyDataAccess.h"

@interface BaseVC () <UIAlertViewDelegate>

@property (strong, nonatomic) UIGestureRecognizer *rotationGesture;
@property (nonatomic, assign) BOOL showingAlertView;

@end

@implementation BaseVC

// *****************************************************************************
#pragma mark -                                          Viewcontroller lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    DLog();
    
    [super viewDidLoad];
    
    self.showingAlertView = NO;
    
    self.rotationGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(userDidRotationGesture)];
    [self.view addGestureRecognizer:self.rotationGesture];
}

// *****************************************************************************
#pragma mark -                                          AlerView delegate
// *****************************************************************************
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self finalizeSurvey];
    }
    
    self.showingAlertView = NO;
}

// *****************************************************************************
#pragma mark -                                          Public methods
// *****************************************************************************
- (void)userDidRotationGesture
{
    if (self.showingAlertView || !self.cancelable)
    {
        return;
    }
    
    if (self.isEndView)
    {
        [self finalizeSurvey];
        
        return;
    }
    
    DLog();
    
    self.showingAlertView = YES;
    
    NSString *language = [NSString stringWithFormat:@"SurveyLang_%@", [SurveyDataAccess sharedManager].languageCode];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"cancel_survey_title", language, nil)
                                                    message:NSLocalizedStringFromTable(@"cancel_survey_message", language, nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedStringFromTable(@"no", language, Nil)
                                          otherButtonTitles:NSLocalizedStringFromTable(@"yes",language, Nil), nil];
    [alert show];
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)finalizeSurvey
{
    //Not all vc have the segue
    
    @try
    {
        [self performSegueWithIdentifier:@"backToPrepare" sender:self];
    }
    @catch (NSException *exception)
    {
        DLog(@"Segue not available.");
    }
    //@finally {
    //}
}

@end

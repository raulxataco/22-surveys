//
//  FirstVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 25/1/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "FirstVC.h"
#import "GlobalHeader.h"
#import "Company.h"
#import "CompanyDataAccess.h"
#import "ServerDataAccess.h"
#import "SurveyDataAccess.h"
#import "WebServices.h"
#import "SVProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "ActivationManager.h"


@interface FirstVC () <UITextFieldDelegate, ActivationManagerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *pinTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIButton *activateButton;
@property (weak, nonatomic) IBOutlet UIView *leftContainer;
@property (weak, nonatomic) IBOutlet UIView *rightContainer;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *publicSurveyButton;
@property (weak, nonatomic) IBOutlet UIButton *privateSurveyButton;
@end

@implementation FirstVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self _makeView];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.pinTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
}

// *****************************************************************************
#pragma mark -                                          Target actions
// *****************************************************************************
- (IBAction)activateApp:(id)sender
{
    if (self.emailTextField.text.length == 0 || self.pinTextField.text.length == 0)
    {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserDefRegisteredUser];

    [[ActivationManager sharedManager] setDelegate:self];
    [[ActivationManager sharedManager] startActivationForEntity:self.pinTextField.text andMail:self.emailTextField.text];
}

- (IBAction)openPublicSurvey:(id)sender
{
    //If Database is not empty then do nothing, just load the main page
    
    Company *company = [[CompanyDataAccess sharedManager] getCompany];
    
    if (company)
    {
        DLog(@"Database already created. Skipping.....");
        
        [self didFinishActivation];
        return;
    }
    
    DLog(@"Creating public database...");

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUserDefRegisteredUser];

    [[ActivationManager sharedManager] setDelegate:self];
    [[ActivationManager sharedManager] startActivationForEntity:@"201400133" andMail:@"xpena@xataco.com"];
}

- (IBAction)unwindToFirst:(UIStoryboardSegue *)unwindSegue
{
    DLog(@"Unwinding....");
}

// *****************************************************************************
#pragma mark -                                        ActivationManager delegate
// *****************************************************************************
- (void)didFinishActivation
{
    [self performSegueWithIdentifier:@"startSurvey" sender:Nil];
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)_makeView
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUserDefRegisteredUser];

    [self.activateButton setTitle:NSLocalizedString(@"first_activate_button_label", Nil) forState:UIControlStateNormal];
    [self.publicSurveyButton setTitle:NSLocalizedString(@"first_public_button_label", Nil) forState:UIControlStateNormal];
    [self.privateSurveyButton setTitle:NSLocalizedString(@"first_private_label", Nil) forState:UIControlStateNormal];

    self.leftContainer.layer.borderColor   = [UIColor whiteColor].CGColor;
    self.rightContainer.layer.borderColor  = [UIColor whiteColor].CGColor;
    self.webView.layer.borderColor         = [UIColor whiteColor].CGColor;

    self.leftContainer.layer.borderWidth   = 1.0;
    self.rightContainer.layer.borderWidth  = 1.0;
    self.webView.layer.borderWidth         = 1.0;

    self.leftContainer.layer.cornerRadius  = 5.0;
    self.rightContainer.layer.cornerRadius = 5.0;
    self.webView.layer.cornerRadius        = 5.0;
    
    self.leftContainer.backgroundColor  = [UIColor clearColor];
    self.rightContainer.backgroundColor = [UIColor clearColor];
    
    //[self.webView loadHTMLString:@"www.google.com" baseURL:[NSURL URLWithString:@"www.google.es"]];
    NSString *locale = [[NSLocale preferredLanguages] firstObject];
    NSString *urlString;

    if ([locale isEqualToString:@"es"] || [locale isEqualToString:@"en"] || [locale isEqualToString:@"ca"])
    {
        urlString = [NSString stringWithFormat:@"http://www.xataco.com/surveys/intro_%@.php", locale];
    }
    else
    {
        urlString = @"http://www.xataco.com/surveys/intro_en.php";
    }
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

@end

//
//  SplashVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 25/1/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "SplashVC.h"
#import "CompanyDataAccess.h"
#import "GlobalHeader.h"

@interface SplashVC ()

@end

@implementation SplashVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    Company *company = [[CompanyDataAccess sharedManager] getCompany];
    
    BOOL userRegistered = [[NSUserDefaults standardUserDefaults] boolForKey:kUserDefRegisteredUser];
    
    if (company && userRegistered)
    {
        [self performSegueWithIdentifier:@"startSurvey" sender:Nil];
    }
    else
    {
        [self performSegueWithIdentifier:@"surveyActivation" sender:Nil];
    }
}

@end

//
//  MainVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 29/09/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "MainVC.h"
#import "CompanyDataAccess.h"
#import "ServerDataAccess.h"
#import "SurveyDataAccess.h"
#import "Company.h"
#import "Utils.h"
#import "DataManager.h"
#import "GlobalHeader.h"
#import "WebServices.h"
#import "SVProgressHUD.h"
#import "SCLAlertView.h"
#import "DataManager.h"
#import "HistoryDataAccess.h"
#import "UploadManager.h"
#import "ActivationManager.h"
#import "FirstVC.h"
#import "CPAnimationSequence.h"

#define kMaxBackgrounds 17

@interface MainVC () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIDynamicAnimatorDelegate>
{
    Company *_currentCompany;
    int _currentBackground;
    NSMutableArray *_surveysList;
    Survey *_currentSurvey;
    BOOL _configOpen;
}
@property (nonatomic, assign) BOOL configUnlocked;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) UIPopoverController *popOver;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sideMenuHorizontalConstraint;
@property (weak, nonatomic) IBOutlet UIView *sideMenuContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *handSwipeImageView;
@property (nonatomic, weak) CPAnimationSequence* animationSequence;

@end

@implementation MainVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _initData];
    [self _makeView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self _closeMenu:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    BOOL tutorialDone = [[NSUserDefaults standardUserDefaults] boolForKey:kUserDefTutorialDone];

    if (!tutorialDone)
    {
        DLog(@"Tutorial NOT done...");
        
        CGPoint originalPoint = self.handSwipeImageView.center;
        CGPoint adjustedPoint = originalPoint;
        adjustedPoint.y -= 100;
        
        self.backgroundImage.userInteractionEnabled = NO;
        
        self.animationSequence = [CPAnimationSequence sequenceWithSteps:
                                  [CPAnimationStep for:1 animate:^{ self.handSwipeImageView.alpha = 0.7;}],
                                  [CPAnimationStep for:1 animate:^{ self.handSwipeImageView.center = adjustedPoint;}],
                                  [CPAnimationStep for:0 animate:^{ [self swipeUpBackground:Nil];}],
                                  [CPAnimationStep after:2 for:1 animate:^{ self.handSwipeImageView.center = originalPoint;}],
                                  [CPAnimationStep for:0 animate:^{ [self swipeDownBackground:Nil];}],
                                  [CPAnimationStep for:1 animate:^{
                                    self.handSwipeImageView.alpha = 0.0;
                                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserDefTutorialDone];
                                    self.backgroundImage.userInteractionEnabled = YES;
                                    }],
                                  nil];
        [self.animationSequence run];
    }
    else
    {
        DLog(@"Tutorial already done...");
    }
}

// *****************************************************************************
#pragma mark -                                  UIImagePickerController Delegate
// *****************************************************************************
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *pickedImage = (UIImage *)[info objectForKey:@"UIImagePickerControllerOriginalImage"];
    NSData *imageData = UIImagePNGRepresentation(pickedImage);
    _currentCompany.backgroundImage = imageData;

    [picker dismissViewControllerAnimated:YES
                               completion:^{
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       if (pickedImage)
                                       {
                                           self.backgroundImage.image = pickedImage;
                                           [DataManager saveContext];
                                       }
                                   });
                               }];
}

// *****************************************************************************
#pragma mark -                                          Target actions
// *****************************************************************************
- (IBAction)openConfig
{
    if (_configOpen)
    {
        return;
    }
    
    if (self.configUnlocked)
    {
        [self _closeMenu:YES];
        
        return;
    }
    
    BOOL userRegistered = [[NSUserDefaults standardUserDefaults] boolForKey:kUserDefRegisteredUser];
    
    if (!userRegistered)
    {
        [self _openMenu];
        return;
    }
    
    _configOpen = YES;
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];

    UITextField *pinCodeField = [alert addTextField:NSLocalizedString(@"main_config_pincode_label", Nil)];
    
    [alert addButton:NSLocalizedString(@"ok", Nil)
          validationBlock:^BOOL{
              if ([pinCodeField.text isEqualToString:[NSString stringWithFormat:@"%@", _currentCompany.idCompany]])
              {
                  return YES;
              }
              else
              {
                  return NO;
              }
          }
              actionBlock:^{
                  [self _openMenu];
                  
                  DLog(@"PIN Correcto");
              }];
    
    [alert addButton:NSLocalizedString(@"lost_pin", Nil) actionBlock:^{
        DLog(@"Recovering password...");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://xataco.com/wp/surveys/"]];
    }];
    
    [alert alertIsDismissed:^{
        _configOpen = NO;
    }];

    [alert showCustom:self
                     image:[UIImage imageNamed:@"Config"]
                     color:[UIColor lightGrayColor]
                     title:NSLocalizedString(@"main_config_title", Nil)
                  subTitle:NSLocalizedString(@"main_config_access_title", Nil)
          closeButtonTitle:NSLocalizedString(@"cancel", Nil)
                  duration:0];
}

- (IBAction)swipeDownBackground:(UISwipeGestureRecognizer *)sender
{
    //if (_currentBackground > 1)
    //{
    //    _currentBackground--;
        [self _refreshBackgroundImageWithAnimation:UIViewAnimationOptionTransitionCurlDown];
    //}
    DLog();
}

- (IBAction)swipeUpBackground:(UISwipeGestureRecognizer *)sender
{
//    if (_currentBackground < kMaxBackgrounds)
//    {
//        _currentBackground++;
        [self _refreshBackgroundImageWithAnimation:UIViewAnimationOptionTransitionCurlUp];
//    }
    DLog();
    
}

- (void)loadSurveyPreparation
{
    if (_configOpen)
    {
        return;
    }
    
    if ([[SurveyDataAccess sharedManager] getFirstSurvey])
    {
        [self performSegueWithIdentifier:@"prepare" sender:self];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Nil
                                                        message:NSLocalizedString(@"main_nosurvey_error", Nil)
                                                       delegate:Nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)loadPhotoGallery:(id)sender
{
    [self _pickImageWithSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (IBAction)loadCamera:(id)sender
{
    if ([Utils doIHaveCam])
    {
        [self _pickImageWithSourceType:UIImagePickerControllerSourceTypeCamera];
    }
}

- (IBAction)loadStats:(id)sender
{
    DLog();
    [self performSegueWithIdentifier:@"stats" sender:self];
}

- (IBAction)downloadSurveys:(id)sender
{
    DLog();
    
    BOOL alreadyRegistered = [[NSUserDefaults standardUserDefaults] boolForKey:kUserDefRegisteredUser];
    
    if (alreadyRegistered)
    {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        //__weak typeof(self) weakSelf = self;
        
        [alert addButton:NSLocalizedString(@"yes", Nil)
             actionBlock:^(void) {
                 [DataManager removeFromEntity:kServerEntity withPredicate:Nil];
                 [DataManager removeFromEntity:kSurveyEntity withPredicate:Nil];
                 [DataManager removeFromEntity:kQuestionEntity withPredicate:Nil];
                 [DataManager removeFromEntity:kAnswer1Entity withPredicate:Nil];
                 [DataManager removeFromEntity:kAnswer2Entity withPredicate:Nil];
                 [DataManager removeFromEntity:kAnswer3Entity withPredicate:Nil];
                 [DataManager removeFromEntity:KHistoryEntity withPredicate:Nil];
                 [DataManager removeFromEntity:kCommentEntity withPredicate:Nil];
                 NSString *idCompany = [NSString stringWithFormat:@"%@", _currentCompany.idCompany];
                 [[ActivationManager sharedManager] startActivationForEntity:idCompany andMail:_currentCompany.email ? _currentCompany.email : @""];
             }];
        
        NSString *message;
        
        //Comprobamos si hay encuestas realizadas pendientes de subir al servidor
        if ([[HistoryDataAccess sharedManager] historyNotUploadedToServer])
        {
            message = NSLocalizedString(@"main_survey_pending_history_message", Nil);
        }
        else
        {
            message = NSLocalizedString(@"main_survey_import_message", Nil);
        }
        
        [alert showWarning:self
                     title:NSLocalizedString(@"main_survey_import_title", nil)
                  subTitle:message
          closeButtonTitle:NSLocalizedString(@"no", Nil)
                  duration:0.0f];
    }
    else
    {
        [self performSegueWithIdentifier:@"backToFirst" sender:self];
    }
}

- (IBAction)closeMenu:(id)sender
{
    [self _closeMenu:YES];
}

// *****************************************************************************
#pragma mark -                                          Private Methods
// *****************************************************************************
- (void)_initData
{
    _currentBackground  = 1;
    _configOpen         = NO;
    self.configUnlocked = NO;
    self.cancelable     = NO;
    
    _currentCompany = [[CompanyDataAccess sharedManager] getCompany];
}

- (void)_makeView
{
    if (_currentCompany)
    {
        self.title= _currentCompany.name;
        
        if (_currentCompany.backgroundImage)
        {
            self.backgroundImage.image = [UIImage imageWithData:_currentCompany.backgroundImage];
        }
    }
    
    [self _addButtons];
    
    self.handSwipeImageView.alpha = 0;
}

- (void)_pickImageWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType = sourceType;

    [self presentViewController:imagePickerController
                       animated:YES
                     completion:nil];
}

- (void)_refreshBackgroundImageWithAnimation:(UIViewAnimationOptions)animationOption
{
    DLog();
    
    int page = _currentBackground;
    
    if (animationOption == UIViewAnimationOptionTransitionCurlUp)
    {
        page++;
    }
    else
    {
        page--;
    }
    
    UIImage *newImage = [UIImage imageNamed:[NSString stringWithFormat:@"standard_background%d", page]];

    if (!newImage) return;
    
    _currentBackground = page;
    
    [UIView transitionWithView:self.backgroundImage
                      duration:1.0f // animation duration
                       options:animationOption
                    animations:^{
                        self.backgroundImage.image = newImage;
                    } completion: Nil];
    
    NSData *imageData = UIImagePNGRepresentation(newImage);
    _currentCompany.backgroundImage = imageData;    
}

- (void)_addButtons
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn addTarget:self action:@selector(openConfig) forControlEvents:UIControlEventTouchUpInside];
    
    btn.bounds = CGRectMake(0, 0, 30, 30);
    [btn setImage:[UIImage imageNamed:@"Config"] forState:UIControlStateNormal];
    
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    self.navigationItem.leftBarButtonItem = buttonItem;
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn2 addTarget:self action:@selector(loadSurveyPreparation) forControlEvents:UIControlEventTouchUpInside];
    
    btn2.bounds = CGRectMake(0, 0, 30, 30);
    [btn2 setImage:[UIImage imageNamed:@"Survey"] forState:UIControlStateNormal];
    
    UIBarButtonItem *buttonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btn2];
    
    self.navigationItem.rightBarButtonItem = buttonItem2;
}

- (void)_openMenu
{
    __weak typeof(self) weakSelf = self;

    weakSelf.configUnlocked = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.sideMenuHorizontalConstraint.constant = 0;
        [UIView animateWithDuration:0.5
                         animations:^{
                             [weakSelf.view layoutIfNeeded];
                         } completion:nil];
    });
}

- (void)_closeMenu:(BOOL)animated
{
    self.sideMenuHorizontalConstraint.constant = -self.sideMenuContainerView.frame.size.width;
    
    if (animated)
    {
        [UIView animateWithDuration:0.5
                         animations:^{
                             [self.view layoutIfNeeded];
                         } completion:nil];
    }
    
    self.configUnlocked = NO;

}

@end

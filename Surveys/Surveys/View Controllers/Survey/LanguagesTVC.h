//
//  LanguagesTVC.h
//  Surveys
//
//  Created by Raúl Blánquez on 22/2/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChangeLanguageyDelegate
- (void)userChangedLanguage:(NSString *)languageCode;
@end

@interface LanguagesTVC : UITableViewController

@property (nonatomic, weak) id<ChangeLanguageyDelegate> delegate;
@property (strong, nonatomic) NSArray *languageList;
@property (strong, nonatomic) NSString *currentLanguageCode;

@end

//
//  LanguageCell.h
//  Surveys
//
//  Created by Raúl Blánquez on 22/2/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "TableCell.h"

@interface LanguageCell : TableCell
@property (weak, nonatomic) IBOutlet UIImageView *flagImage;
@property (weak, nonatomic) IBOutlet UILabel *languageNameLabel;
@property (nonatomic, assign) NSInteger languageId;
@property (nonatomic, strong) NSString *languageCode;

@end

//
//  QuestionType2VC.m
//  Surveys
//
//  Created by Raúl Blánquez on 04/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "QuestionType2VC.h"
#import "GlobalHeader.h"
#import "AnswerFormat3.h"
#import "SurveyDataAccess.h"
#import "AnswerButton.h"

@interface QuestionType2VC ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet AnswerButton *answer1Button;
@property (weak, nonatomic) IBOutlet AnswerButton *answer2Button;

@end

@implementation QuestionType2VC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _makeView];
}

// *****************************************************************************
#pragma mark -                                          Target actions
// *****************************************************************************
- (IBAction)didSelectAnswer:(UIButton *)sender
{    
    [self _resetAnswers];
    
    sender.selected = YES;
    
    self.canContinue = YES;
    
    self.answer = [NSNumber numberWithInteger:sender.tag+1];
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)_makeView
{
    self.questionLabel.textColor = [UIColor whiteColor];
    self.questionLabel.text = self.question.question;
    
    NSString *language = [NSString stringWithFormat:@"SurveyLang_%@", [SurveyDataAccess sharedManager].languageCode];
    //if (self.question.answerFormat3.option1.length == 0)
    //{
        self.question.answerFormat3.option1 = NSLocalizedStringFromTable(@"yes", language, Nil);
    //}
    //if (self.question.answerFormat3.option2.length == 0)
    //{
        self.question.answerFormat3.option2 = NSLocalizedStringFromTable(@"no", language, Nil);
    //}

    [self.answer1Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.answer1Button setTitle:self.question.answerFormat3.option1 forState:UIControlStateNormal];

    [self.answer2Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.answer2Button setTitle:self.question.answerFormat3.option2 forState:UIControlStateNormal];
}

- (void)_resetAnswers
{
    self.answer1Button.selected = NO;
    self.answer2Button.selected = NO;
}

@end

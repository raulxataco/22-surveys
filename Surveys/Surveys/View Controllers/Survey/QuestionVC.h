//
//  QuestionVC.h
//  Surveys
//
//  Created by Raúl Blánquez on 04/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

@protocol QuestionDelegate
- (void)loadNextQuestion;
@end

@interface QuestionVC : BaseVC

@property (nonatomic, weak) id<QuestionDelegate> delegate;
@property (nonatomic, assign) BOOL canContinue;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestionButton;


@end

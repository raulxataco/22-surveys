//
//  LanguagesTVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 22/2/15.
//  Copyright (c) 2015 Xataco. All rights reserved.
//

#import "LanguagesTVC.h"
#import "LanguageCell.h"
#import "LanguageDataAccess.h"
#import "UIImageView+AFNetworking.h"
#import "GlobalHeader.h"

#define kURLForFlags @"http://xataco.com/surveys/images/flag_XX.png"

@interface LanguagesTVC ()
{
    NSInteger _currentSelectedRow;
    NSInteger _indexOfDeviceLocale;
    NSIndexPath *_indexPathForCellToHighlight;
}

@end

@implementation LanguagesTVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    DLog();
    
    [super viewDidLoad];
    
    _indexOfDeviceLocale = [self.languageList indexOfObject:[LanguageDataAccess getLanguage:self.currentLanguageCode]];
    
    _indexPathForCellToHighlight = [NSIndexPath indexPathForRow:_indexOfDeviceLocale inSection:0];
}

// *****************************************************************************
#pragma mark -                                          Table view data source
// *****************************************************************************
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.languageList.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath isEqual:_indexPathForCellToHighlight])
    {
        //[self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
        cell.selected = YES;
        _currentSelectedRow = indexPath.row;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LanguageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"languageCell" forIndexPath:indexPath];
    
    Language *language = self.languageList[indexPath.row];
    
    cell.languageNameLabel.text = language.native;
    cell.languageCode = language.languageCode;
    //cell.flagImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"flag_%@", language.languageCode]];
    NSString *urlString = [kURLForFlags stringByReplacingOccurrencesOfString:@"XX" withString:language.languageCode];
    NSURL *url = [NSURL URLWithString:urlString];
    [cell.flagImage setImageWithURL:url placeholderImage:[UIImage imageNamed:Nil]];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

// *****************************************************************************
#pragma mark -                                          Table view delegate
// *****************************************************************************
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:_currentSelectedRow inSection:0];
    [self.tableView deselectRowAtIndexPath:oldIndexPath animated:YES];
    LanguageCell *cell = (LanguageCell *)[tableView cellForRowAtIndexPath:oldIndexPath];
    cell.selected = NO;
    
    _currentSelectedRow = indexPath.row;
    cell = (LanguageCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    [self.delegate userChangedLanguage:cell.languageCode];
}

// *****************************************************************************
#pragma mark -                                          Private Methods
// *****************************************************************************

@end

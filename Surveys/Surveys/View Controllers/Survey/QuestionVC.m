//
//  QuestionVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 04/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "QuestionVC.h"
#import "GlobalHeader.h"
#import "SVProgressHUD.h"
#import "SurveyDataAccess.h"

@interface QuestionVC ()

@end

@implementation QuestionVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    DLog();
    
    [super viewDidLoad];
    
    [self.nextQuestionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    NSString *language = [NSString stringWithFormat:@"SurveyLang_%@", [SurveyDataAccess sharedManager].languageCode];
    [self.nextQuestionButton setTitle:NSLocalizedStringFromTable(@"next", language, Nil) forState:UIControlStateNormal];
    
    self.canContinue = NO;
    self.cancelable  = YES;
}

- (BOOL)canContinue
{
    DLog(@"%d", _canContinue);
    
    return _canContinue;
}

// *****************************************************************************
#pragma mark -                                          Target Actions
// *****************************************************************************
- (IBAction)loadNextQuestion:(UIButton *)sender
{
    DLog();
    
    if (self.canContinue)
    {
        [self.delegate loadNextQuestion];
    }
    else
    {
        NSString *language = [NSString stringWithFormat:@"SurveyLang_%@", [SurveyDataAccess sharedManager].languageCode];
        [SVProgressHUD showErrorWithStatus:NSLocalizedStringFromTable(@"survey_select_answer", language, Nil)];
    }
}

@end

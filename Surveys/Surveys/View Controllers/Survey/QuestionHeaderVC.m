//
//  QuestionHeaderVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 04/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "QuestionHeaderVC.h"
#import "QuestionVC.h"
#import "QuestionType1VC.h"
#import "QuestionType2VC.h"
#import "QuestionType3VC.h"
#import "QuestionType4VC.h"
#import "QuestionType5VC.h"
#import "EndVC.h"
#import "GlobalHeader.h"
#import "CompanyDataAccess.h"
#import "SurveyDataAccess.h"
#import "HistoryDataAccess.h"

@interface QuestionHeaderVC () <QuestionDelegate>
{
    NSMutableArray *_questionList;
    NSInteger _currentQuestionIndex;
    NSInteger _maxQuestions;
    UIViewController *_currentVC;
}
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *questionCounterLabel;
@property (weak, nonatomic) IBOutlet UIView *questionContainerView;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *copyrightLabel;

@end

@implementation QuestionHeaderVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    DLog();
    
    [super viewDidLoad];
    
    [self _initData];
    [self _makeView];
}

// *****************************************************************************
#pragma mark -                                          Question delegate
// *****************************************************************************
- (void)loadNextQuestion
{
    if (_currentQuestionIndex == _maxQuestions - 1)
    {
        [self _storeQuestionsInHistory];        
    }
    
    [self _loadViewControllerAtIndex:++_currentQuestionIndex];
    [self _updateHeader];
}

- (void)reloadMePlease
{
    [self _loadViewControllerAtIndex:_currentQuestionIndex];
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)_initData
{
    DLog();
    
    _currentQuestionIndex = 0;
        
    NSArray *questions = [[SurveyDataAccess sharedManager] getQuestionsForCurrentLanguage];
    
    _questionList = [[NSMutableArray alloc] initWithCapacity:questions.count + 1];
    
    for (Question *question in questions)
    {
        ServerQuestionType format = (ServerQuestionType)[question.format integerValue];
        
        switch (format) {
            case kQuestion5Answers:
            {
                QuestionType1VC *questionType1VC = [self.storyboard instantiateViewControllerWithIdentifier:@"questionType1"];
                questionType1VC.question = question;
                questionType1VC.delegate = self;
                
                [_questionList addObject:questionType1VC];

                break;
            }
            case kQuestion3Answers:
            {
                QuestionType5VC *questionType5VC = [self.storyboard instantiateViewControllerWithIdentifier:@"questionType5"];
                questionType5VC.question = question;
                questionType5VC.delegate = self;
                
                [_questionList addObject:questionType5VC];
                
                break;
            }
            case kQuestion2Answers:
            {
                QuestionType2VC *questionType2VC = [self.storyboard instantiateViewControllerWithIdentifier:@"questionType2"];
                questionType2VC.question = question;
                questionType2VC.delegate = self;
                
                [_questionList addObject:questionType2VC];

                break;
            }
            case kQuestionComments:
            {
                QuestionType4VC *questionType4VC = [self.storyboard instantiateViewControllerWithIdentifier:@"questionType4"];
                questionType4VC.question = question;
                questionType4VC.delegate = self;
                
                [_questionList addObject:questionType4VC];
                break;
            }
            case kQuestionStars:
            {
                QuestionType3VC *questionType3VC = [self.storyboard instantiateViewControllerWithIdentifier:@"questionType3"];
                questionType3VC.question = question;
                questionType3VC.delegate = self;
                
                [_questionList addObject:questionType3VC];
                break;
            }
            default:
                DLog(@"Incorrect question format");
                break;
        }
    }
    
    EndVC *endVC = [self.storyboard instantiateViewControllerWithIdentifier:@"end"];
    endVC.delegate = self;
    
    [_questionList addObject:endVC];
    
    _maxQuestions = _questionList.count - 1;
}

- (void)_makeView
{
    Company *company = [[CompanyDataAccess sharedManager] getCompany];
    
    self.title= company.name;
    
    if (company.backgroundImage)
    {
        self.backgroundImage.image = [UIImage imageWithData:company.backgroundImage];
    }

    self.copyrightLabel.hidden = YES;
    
    [self _updateHeader];
    
    _currentVC = _questionList[_currentQuestionIndex];
    [self addChildViewController:_currentVC];
    _currentVC.view.frame = self.questionContainerView.bounds;
    [self.questionContainerView addSubview:_currentVC.view];
}

- (void)_loadViewControllerAtIndex:(NSInteger)index
{
    DLog(@"index = %d", (int)index);
    
    UIViewController *destinationVC = _questionList[index];
    [self addChildViewController:destinationVC];
    
    [self transitionFromViewController:_currentVC
                      toViewController:destinationVC
                              duration:0.5
                               options:UIViewAnimationOptionTransitionFlipFromBottom
                            animations:^{
                                [_currentVC.view removeFromSuperview];
                                destinationVC.view.frame = self.questionContainerView.bounds;
                                [self.questionContainerView addSubview:_currentVC.view];
                            }
                            completion:^(BOOL finished) {
                                [destinationVC didMoveToParentViewController:self];
                                [_currentVC removeFromParentViewController];
                                _currentVC = destinationVC;
                            }];
}

- (void)_updateHeader
{
    NSString *language = [NSString stringWithFormat:@"SurveyLang_%@", [SurveyDataAccess sharedManager].languageCode];

    if (_currentQuestionIndex == _maxQuestions - 1)
    {
        self.questionCounterLabel.text = NSLocalizedStringFromTable(@"survey_last_question", language, Nil);
    }
    else
    if (_currentQuestionIndex < _maxQuestions)
    {
        self.questionCounterLabel.text = [NSString stringWithFormat:NSLocalizedStringFromTable(@"survey_question_counter", language, Nil), (int)_currentQuestionIndex+1, (int)_maxQuestions];
    }
    else
    {
        self.questionCounterLabel.text = @"";
        self.copyrightLabel.hidden     = NO;
    }
}

- (void)_storeQuestionsInHistory
{
    for (UIViewController *vc in _questionList)
    {
        if ([vc isKindOfClass:NSClassFromString(@"QuestionType1VC")])
        {
            QuestionType1VC *vc1 = (QuestionType1VC *)vc;
            [[HistoryDataAccess sharedManager] setHistoryForQuestion:vc1.question andANswer:vc1.answer];
            
            continue;
        }
        if ([vc isKindOfClass:NSClassFromString(@"QuestionType2VC")])
        {
            QuestionType2VC *vc2 = (QuestionType2VC *)vc;
            [[HistoryDataAccess sharedManager] setHistoryForQuestion:vc2.question andANswer:vc2.answer];

            continue;
        }
        if ([vc isKindOfClass:NSClassFromString(@"QuestionType3VC")])
        {
            QuestionType3VC *vc3 = (QuestionType3VC *)vc;
            [[HistoryDataAccess sharedManager] setHistoryForQuestion:vc3.question andANswer:vc3.answer];
            
            continue;
        }
        if ([vc isKindOfClass:NSClassFromString(@"QuestionType4VC")])
        {
            QuestionType4VC *vc4 = (QuestionType4VC *)vc;
            [[HistoryDataAccess sharedManager] setHistoryForQuestion:vc4.question andComment:vc4.answer];
            
            continue;
        }
        if ([vc isKindOfClass:NSClassFromString(@"QuestionType5VC")])
        {
            QuestionType5VC *vc5 = (QuestionType5VC *)vc;
            [[HistoryDataAccess sharedManager] setHistoryForQuestion:vc5.question andANswer:vc5.answer];
        }
    }
}

@end

//
//  QuestionType5VC.m
//  Surveys
//
//  Created by mobdev1 on 07/11/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "QuestionType5VC.h"

#import "GlobalHeader.h"
#import "AnswerFormat2.h"
#import "SurveyDataAccess.h"
#import "AnswerButton.h"

@interface QuestionType5VC ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet AnswerButton *answer1Button;
@property (weak, nonatomic) IBOutlet AnswerButton *answer2Button;
@property (weak, nonatomic) IBOutlet AnswerButton *answer3Button;

@end

@implementation QuestionType5VC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _makeView];
}

// *****************************************************************************
#pragma mark -                                          Target actions
// *****************************************************************************
- (IBAction)didSelectAnswer:(AnswerButton *)sender
{
    [self _resetAnswers];
    
    sender.selected = YES;
    
    self.canContinue = YES;
    
    self.answer = [NSNumber numberWithInt:(int)sender.tag+1];
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)_makeView
{
    self.questionLabel.textColor = [UIColor whiteColor];
    self.questionLabel.text = self.question.question;
    
    [self.answer1Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.answer1Button setTitle:self.question.answerFormat2.option1 forState:UIControlStateNormal];
    
    [self.answer2Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.answer2Button setTitle:self.question.answerFormat2.option2 forState:UIControlStateNormal];
    
    [self.answer3Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.answer3Button setTitle:self.question.answerFormat2.option3 forState:UIControlStateNormal];
}

- (void)_resetAnswers
{
    self.answer1Button.selected = NO;
    self.answer2Button.selected = NO;
    self.answer3Button.selected = NO;
}

@end

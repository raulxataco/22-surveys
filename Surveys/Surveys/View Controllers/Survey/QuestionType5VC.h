//
//  QuestionType5VC.h
//  Surveys
//
//  Created by mobdev1 on 07/11/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "QuestionVC.h"
#import "Question.h"

@interface QuestionType5VC : QuestionVC

@property (nonatomic, strong) Question *question;
@property (nonatomic, strong) NSNumber *answer;

@end

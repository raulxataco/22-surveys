//
//  SurveyStartVC.m
//  Surveys
//
//  Created by Raúl Blánquez on 01/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "SurveyStartVC.h"
#import "CompanyDataAccess.h"
#import "SurveyDataAccess.h"
#import "LanguageDataAccess.h"
#import "SVProgressHUD.h"
#import "GlobalHeader.h"
#import "LanguagesTVC.h"

@interface SurveyStartVC () <ChangeLanguageyDelegate>
{
    Company *_company;
    Survey *_survey;
}
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *surveyTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *surveyInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *surveyTimeWarningLabel;

@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UITableView *languageTableView;
@property (strong, nonatomic) LanguagesTVC *languagesTVC;

@end

@implementation SurveyStartVC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    DLog();
    
    [super viewDidLoad];
    
    self.cancelable = YES;
    
    [self _makeView];
}

// *****************************************************************************
#pragma mark -                                          Custom getters & setters
// *****************************************************************************
- (LanguagesTVC *)languagesTVC
{
    if (!_languagesTVC)
    {
        _languagesTVC = [[LanguagesTVC alloc] init];
    }
    
    return _languagesTVC;
}

// *****************************************************************************
#pragma mark -                                          ChangeLanguageDelegate
// *****************************************************************************
- (void)userChangedLanguage:(NSString *)languageCode
{
    [self _updateViewWithLanguage:languageCode];
    [[SurveyDataAccess sharedManager] setLanguageCode:languageCode];
}

// *****************************************************************************
#pragma mark -                                          Target actions
// *****************************************************************************
//- (IBAction)changeSurveyLanguage:(UIButton *)sender
//{
//    if ([[SurveyDataAccess sharedManager] getSurveyWithLanguageId:[NSNumber numberWithInteger:sender.tag]])
//    {
//        [self _updateViewWithLanguage:sender.tag];
//        
//        [[SurveyDataAccess sharedManager] setLanguageId:sender.tag];
//    }
//    else
//    {
//        NSString *language = [NSString stringWithFormat:@"SurveyLang%d", (int)[SurveyDataAccess sharedManager].languageId];
//        [SVProgressHUD showErrorWithStatus:NSLocalizedStringFromTable(@"info_survey_language_not_available", language, Nil)];
//    }
//}

- (IBAction)unwindToStart:(UIStoryboardSegue *)unwindSegue
{
    DLog();
}

// *****************************************************************************
#pragma mark -                                          Private Methods
// *****************************************************************************
- (void)_localizeLabels:(NSString *)languageCode
{
    NSString *language               = [NSString stringWithFormat:@"SurveyLang_%@", languageCode];
    [self.startButton setTitle:NSLocalizedStringFromTable(@"info_survey_start", language, Nil) forState:UIControlStateNormal];
    
    self.surveyTimeWarningLabel.text = [NSString stringWithFormat:NSLocalizedStringFromTable(@"survey_start_warning", language, nil),
                                        [NSNumber numberWithInteger:[[SurveyDataAccess sharedManager] getNumberOfQuestionsWithLanguageCode:@"es"]]];
    self.surveyTimeWarningLabel.text = [[CompanyDataAccess sharedManager] getWellcomeMessageForLanguage:languageCode];
    self.surveyInfoLabel.text        = NSLocalizedStringFromTable(@"info_survey_select_language", language, Nil);
}

- (void)_makeView
{
    _company = [[CompanyDataAccess sharedManager] getCompany];
    
    self.title= _company.name;
    
    if (_company.backgroundImage)
    {
        self.backgroundImage.image = [UIImage imageWithData:_company.backgroundImage];
    }
    
    NSString *locale = [[NSLocale preferredLanguages] firstObject];
    //NSNumber *languageId = [LanguageDataAccess getLanguageIdForLanguageCode:locale];
    
    self.languageTableView.dataSource = self.languagesTVC;
    self.languageTableView.delegate   = self.languagesTVC;
    self.languagesTVC.languageList    = [LanguageDataAccess getLanguages];
    self.languagesTVC.currentLanguageCode = locale;
    self.languagesTVC.view      = self.languagesTVC.tableView;
    self.languagesTVC.delegate  = self;
    
    [[SurveyDataAccess sharedManager] setLanguageCode:locale];

    [self _updateViewWithLanguage:locale];
}

- (void)_updateViewWithLanguage:(NSString *)languageCode
{
    //_survey = [[SurveyDataAccess sharedManager] getSurveyWithLanguageId:[NSNumber numberWithInteger:languageId]];
    _survey = [[SurveyDataAccess sharedManager] getSurveyWithLanguageCode:languageCode];
    
    self.surveyTitleLabel.text = _survey.title;
    
    [self _localizeLabels:languageCode];
}

@end

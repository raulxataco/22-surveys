//
//  QuestionType3VC.m
//  Surveys
//
//  Created by Raúl Blánquez on 04/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "QuestionType3VC.h"

@interface QuestionType3VC ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnStars;

@end

@implementation QuestionType3VC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _makeView];
}

// *****************************************************************************
#pragma mark -                                          Target actions
// *****************************************************************************
- (IBAction)didSelectStar:(UIButton *)sender
{
    NSInteger valor = sender.tag;
    
    //In order to set a rating of 0, I deactivate first button only and only its the active
    if (sender.tag == 1 && sender.selected && ![self _thereAreMoreStars])
    {
        sender.selected = NO;
        
        for (UIButton *b in self.btnStars)
        {
            if (b.tag < valor)
                b.selected = YES;
            else
                b.selected = NO;
        }
    }
    else
    {
        for (UIButton *b in self.btnStars)
        {
            if (b.tag <= valor)
                b.selected = YES;
            else
                b.selected = NO;
        }
    }
    
    self.canContinue = YES;
    
    self.answer = [NSNumber numberWithInteger:sender.tag+1];
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)_makeView
{
    self.questionLabel.textColor = [UIColor whiteColor];
    self.questionLabel.text = self.question.question;
}

//Returns YES if current Rating is greater than 1
- (BOOL)_thereAreMoreStars
{
    BOOL more = NO;
    
    for (UIButton *b in self.btnStars)
    {
        if (b.tag > 1 && b.selected)
        {
            more = YES;
            break;
        }
    }
    
    return more;
}

@end

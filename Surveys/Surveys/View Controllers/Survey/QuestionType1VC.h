//
//  QuestionType1VC.h
//  Surveys
//
//  Created by Raúl Blánquez on 04/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "QuestionVC.h"
#import "Question.h"

@interface QuestionType1VC : QuestionVC

@property (nonatomic, strong) Question *question;
@property (nonatomic, strong) NSNumber *answer;

@end

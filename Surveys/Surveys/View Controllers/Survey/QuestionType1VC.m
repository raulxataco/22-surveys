//
//  QuestionType1VC.m
//  Surveys
//
//  Created by Raúl Blánquez on 04/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "QuestionType1VC.h"
#import "GlobalHeader.h"
#import "AnswerFormat1.h"
#import "AnswerFormat2.h"
#import "AnswerFormat3.h"
#import "SurveyDataAccess.h"
#import "AnswerButton.h"

@interface QuestionType1VC ()

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet AnswerButton *answer1Button;
@property (weak, nonatomic) IBOutlet AnswerButton *answer2Button;
@property (weak, nonatomic) IBOutlet AnswerButton *answer3Button;
@property (weak, nonatomic) IBOutlet AnswerButton *answer4Button;
@property (weak, nonatomic) IBOutlet AnswerButton *answer5Button;
@end

@implementation QuestionType1VC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _makeView];
}

// *****************************************************************************
#pragma mark -                                          Target actions
// *****************************************************************************
- (IBAction)didSelectAnswer:(AnswerButton *)sender
{    
    [self _resetAnswers];
    
    sender.selected = YES;
    
    self.canContinue = YES;
    
    self.answer = [NSNumber numberWithInteger:sender.tag+1];
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)_makeView
{
    self.questionLabel.textColor = [UIColor whiteColor];
    self.questionLabel.text = self.question.question;
    
    [self.answer1Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.answer1Button setTitle:self.question.answerFormat1.option1 forState:UIControlStateNormal];
    
    [self.answer2Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.answer2Button setTitle:self.question.answerFormat1.option2 forState:UIControlStateNormal];

    [self.answer3Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.answer3Button setTitle:self.question.answerFormat1.option3 forState:UIControlStateNormal];

    [self.answer4Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.answer4Button setTitle:self.question.answerFormat1.option4 forState:UIControlStateNormal];

    [self.answer5Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.answer5Button setTitle:self.question.answerFormat1.option5 forState:UIControlStateNormal];
}

- (void)_resetAnswers
{
    self.answer1Button.selected = NO;
    self.answer2Button.selected = NO;
    self.answer3Button.selected = NO;
    self.answer4Button.selected = NO;
    self.answer5Button.selected = NO;
}

@end

//
//  QuestionType4VC.m
//  Surveys
//
//  Created by Raúl Blánquez on 04/10/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "QuestionType4VC.h"
#import "EndVC.h"
#import "GlobalHeader.h"

@interface QuestionType4VC () <UITextViewDelegate>
{
}
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UITextView *commentsText;

@end

@implementation QuestionType4VC

// *****************************************************************************
#pragma mark -                                          ViewController lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _makeView];
    
    self.canContinue = YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// *****************************************************************************
#pragma mark -                                          Textview delegate
// *****************************************************************************
- (void)textViewDidChange:(UITextView *)textView
{
    DLog(@"%@", textView.text);
    self.answer = textView.text;
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)_makeView
{
    self.questionLabel.textColor = [UIColor whiteColor];
    self.commentsText.textColor = [UIColor whiteColor];
    self.commentsText.text = @"";
    
    [self.commentsText becomeFirstResponder];
    
}

@end
